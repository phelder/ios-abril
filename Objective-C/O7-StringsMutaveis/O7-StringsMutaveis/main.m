//
//  main.m
//  O7-StringsMutaveis
//
//  Created by Helder Pereira on 21/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    NSMutableString *mutie = [[NSMutableString alloc] init];
    
    [mutie appendString:@"Olá"];
    [mutie appendString:@" a"];
    [mutie appendString:@" Todos"];
    [mutie appendString:@"!!!!!!!!"];
    
    NSLog(@"%@", mutie);


//    NSMutableString *teste = [NSMutableString stringWithString:@"Bananas"];
//    for (int i = 0; i < teste.length; i++) {
//        
//        if (i != 0) {
//            [teste insertString:@"-" atIndex:i];
//            i++;
//        }
//        
//    }
//    NSLog(@"%@", teste);
    
//    NSString *teste = @"bananas";
//    NSMutableString *testeComHifens = [[NSMutableString alloc] init];
//    
//    for (int i = 0; i < teste.length; i++) {
//        
//        char c = [teste characterAtIndex:i];
//        
//        if (i != 0) {
//            [testeComHifens appendString:@"-"];
//        }
//        [testeComHifens appendFormat:@"%c", c];
//    }
//    NSLog(@"TH: %@", testeComHifens);
    
    
    
    NSString *teste = @"bananas";
    NSMutableString *testeComHifens = [[NSMutableString alloc] init];
    
    for (int i = 0; i < teste.length; i++) {
    
//        NSRange range;
//        range.location = i;
//        range.length = 1;

        NSRange range = NSMakeRange(i, 1);
        NSString *substring = [teste substringWithRange:range];
        
        if (i != 0) {
            [testeComHifens appendString:@"-"];
        }
        [testeComHifens appendString:substring];
    }
    NSLog(@"TH: %@", testeComHifens);    
    
    return 0;
}
