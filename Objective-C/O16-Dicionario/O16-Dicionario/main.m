//
//  main.m
//  O16-Dicionario
//
//  Created by Helder Pereira on 23/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    // STATUS (ON / OFF)
    // RANGE_MIN :
    // RANGE_MAX :
    // VALOR
    
    NSDictionary *options = @{
                            @"status": @(YES),
                            @"min": @(1),
                            @"max": @(100),
                            @"currentValue": @(50),
                            @"name": @"sliderIdade"
                            };
    
    //[options objectForKey:@"status"];
    //options[@"status"];
    
    NSLog(@"%@", options);
    
    for (NSString *key in options) {
        NSLog(@"%@: %@", key, options[key]);
    }
    
    return 0;
}
