//
//  main.m
//  O12-MetodosDaClasse
//
//  Created by Helder Pereira on 23/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Aluno.h"

int main(int argc, const char * argv[]) {
    
    NSArray<Pessoa *> *gente;
    gente = @[
              
  [Pessoa pessoaWithNome:@"H" idade:@(3) cidade:@"P"],
  [Pessoa pessoaWithNome:@"H" idade:@(3) cidade:@"P"],
  [Pessoa pessoaWithNome:@"H" idade:@(3) cidade:@"P"],
  [Pessoa pessoaWithNome:@"H" idade:@(3) cidade:@"P"],
  [Pessoa pessoaWithNome:@"H" idade:@(3) cidade:@"P"],
  [Pessoa pessoaWithNome:@"H" idade:@(3) cidade:@"P"],
  [Pessoa pessoaWithNome:@"H" idade:@(3) cidade:@"P"],
  [Pessoa pessoaWithNome:@"H" idade:@(3) cidade:@"P"],
  [Aluno alunoWithNome:@"Z" idade:@(30) cidade:@"L" turma:@"X" notaFinal:@(10)]
  
  ];
    
    
    return 0;
}
