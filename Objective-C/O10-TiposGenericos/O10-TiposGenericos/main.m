//
//  main.m
//  O10-TiposGenericos
//
//  Created by Helder Pereira on 23/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Aluno.h"

int main(int argc, const char * argv[]) {
    
    
    NSArray<NSNumber *> *idades = @[
                        @(35),
                        @(36),
                        @(28),
                        @(24),
                        @(24),
                        @(31),
                        @(35)
                        ];
    
    // CRIAR ARRAY E PREENCHE-LO COM
    // OBJECTOS PESSOA
    // E OBJECTOS ALUNOS (SUBCLASSE PESSOA)
    // NO FIM FAZER UM ALGORITMO QUE CONTA
    // QUANTAS DAS PESSOAS TB SAO ALUNOS
    NSArray<Pessoa *> *gente;
    
    gente = @[
             [[Pessoa alloc] init],
             [[Pessoa alloc] init],
             [[Aluno alloc] init],
             [[Aluno alloc] init],
             [[Pessoa alloc] init],
             [[Pessoa alloc] init],
             [[Aluno alloc] init]
             ];
    
    int contadorAlunos = 0;
    for (int i = 0; i < gente.count; i++) {
    
        Pessoa *p = gente[i];
        if ([p isKindOfClass:[Aluno class]]) {
            contadorAlunos++;
        }
    }
    
    NSLog(@"Existem %d alunos", contadorAlunos);
    
    
    
    
    return 0;
}
