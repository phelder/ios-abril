//
//  Pessoa.h
//  O9-Heranca
//
//  Created by Helder Pereira on 23/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pessoa : NSObject

@property (strong, nonatomic) NSString *nome;
@property (strong, nonatomic) NSNumber *idade;
@property (strong, nonatomic) NSString *cidade;

@end
