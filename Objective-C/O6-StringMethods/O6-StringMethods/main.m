//
//  main.m
//  O6-StringMethods
//
//  Created by Helder Pereira on 21/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    NSString *texto1 = @""; // #123ABC
    
    NSLog(@"T1: %@", texto1);
    
    texto1 = @"Olá a todos!!!"; //F2A456
    
    NSLog(@"T1: %@", texto1);
    
    texto1 = [texto1 stringByAppendingString:@" Está tudo?"];
    
    NSLog(@"T1: %@", texto1);
    
    NSString *t1 = @"A";
    NSString *t2 = @"B";
    NSString *t3 = @"C";
    NSString *t4 = @"D";
    
    NSString *tTudo = [[NSString alloc] initWithFormat:@"%@ - %@ - %@ - %@", t1, t2,t3, t4];
    
    NSLog(@"%@", tTudo);
    
    // ATALHO PARA NAO TER QUE ESCREVER ALLOC INIT...
    NSString *tOutra = [NSString stringWithFormat:@"%@ - %@ - %@ - %@", t1, t2,t3, t4];

    
    return 0;
}
