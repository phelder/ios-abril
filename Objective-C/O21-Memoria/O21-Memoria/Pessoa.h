//
//  Pessoa.h
//  O21-Memoria
//
//  Created by Helder Pereira on 28/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pessoa : NSObject

@property (retain, nonatomic) NSString *nome;

@end
