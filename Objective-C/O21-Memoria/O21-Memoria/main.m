//
//  main.m
//  O21-Memoria
//
//  Created by Helder Pereira on 28/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"


int main(int argc, const char * argv[]) {
    
    // ARC (AUTOMATIC REFERENCE COUNTING)
    
    // EXEMPLO COM ARC DESLIGADO
    
    NSMutableArray *ma = [[NSMutableArray alloc] init]; // +1
    
    [ma retain]; // +1
    
    [ma release]; // -1
    
    [ma release]; // -1
    
    [ma retainCount];
    
    NSString *teste = [[NSString alloc] initWithFormat:@"%d", 12];
    
    Pessoa *p = [[Pessoa alloc] init];
    
    p.nome = teste; //[p setNome:teste];
    
    
    @autoreleasepool {
        
        Pessoa *p2 = [[[Pessoa alloc] init] autorelease]; // 0
        Pessoa *p3 = [[[Pessoa alloc] init] autorelease]; // 1
        Pessoa *p4 = [[Pessoa alloc] init]; // 1
        
        [p3 retain];
    }
    
    return 0;
}
