//
//  main.m
//  O11-ArraysMutaveis
//
//  Created by Helder Pereira on 23/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    NSMutableArray *teste = [[NSMutableArray alloc] init];
    
    [teste addObject:@"Ze"]; // 0
    [teste addObject:@"Tobias"]; // 1
    [teste addObject:@"Tomé"]; // 2
    [teste addObject:@"Barnabé"]; // 3
    
    NSLog(@"TAM: %d", (int)teste.count);
    
    for (int i = 0; i < teste.count; i++) {
    
        NSLog(@"%@", teste[i]);
    
    }
    
    
    NSLog(@"FIM!!!!");
    
    return 0;
}
