//
//  Pessoa.m
//  O5-FamiliaInit
//
//  Created by Helder Pereira on 21/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@implementation Pessoa

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        _nome = @"nome por defeito";
        _idade = 0;
        _cidade = @"cidade por defeito";
        
        NSLog(@"YAY UMA PESSOA!!!");
    }
    return self;
}

- (instancetype)initWithNome:(NSString *)nome idade:(int)idade cidade:(NSString *)cidade
{
    self = [super init];
    if (self) {
        _nome = nome;
        _idade = idade;
        _cidade = cidade;
    }
    return self;
}

//- (void)setNome:(NSString *)nome
// void setValor(int a)
// void setValores(int a, int b);

- (void)set:(NSString *)nome :(int)idade :(NSString *)cidade
{
    _nome = nome;
    _idade = idade;
    _cidade = cidade;
}

//setNome: eIdade: eJaAgoraCidade:

- (void)setNome:(NSString *)nome eIdade:(int)idade eJaAgoraCidade:(NSString *)cidade
{
    _nome = nome;
    _idade = idade;
    _cidade = cidade;
}


@end
