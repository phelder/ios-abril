//
//  main.m
//  O5-FamiliaInit
//
//  Created by Helder Pereira on 21/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
    
    Pessoa *p = [[Pessoa alloc] init];
    
    //p.nome; // [p nome];
    
    p.nome = @"T"; // [p setNome:@"T"];
    
    [p set:@"Tobias" :12 :@"Lisboa"];
    
    [p setNome:@"Tomás" eIdade:20 eJaAgoraCidade:@"Porto"];
    
    Pessoa *p2 = [[Pessoa alloc] initWithNome:@"Zeferino" idade:15 cidade:@"Braga"];
    
    return 0;
}
