//
//  Pessoa.h
//  O5-FamiliaInit
//
//  Created by Helder Pereira on 21/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pessoa : NSObject

@property (strong, nonatomic) NSString *nome;
@property (assign, nonatomic) int idade;
@property (strong, nonatomic) NSString *cidade;


- (instancetype)initWithNome:(NSString *)nome idade:(int)idade cidade:(NSString *)cidade;

- (void)set:(NSString *)nome :(int)idade :(NSString *)cidade;

- (void)setNome:(NSString *)nome eIdade:(int)idade eJaAgoraCidade:(NSString *)cidade;

@end
