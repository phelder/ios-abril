//
//  main.m
//  O9-Heranca
//
//  Created by Helder Pereira on 23/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Aluno.h"

int main(int argc, const char * argv[]) {
    
    Pessoa *p = [[Pessoa alloc] init];
    p.nome = @"Tobias";
    p.idade = @(20);
    p.cidade = @"Aveiro";
    
    Aluno *a1 = [[Aluno alloc] init];
    a1.nome = @"Luís";
    a1.idade = @(24);
    a1.cidade = @"Porto";
    a1.turma = @"WMD";
    a1.notaFinal = @(0);
    
    
    
    Pessoa *p2 = [[Aluno alloc] init];
    
    p2.nome = @"Ricardo";
    p2.cidade = @"Aveiro";
    p2.idade = @(35);
    
    ((Aluno *)p2).turma = @"WMD";
    ((Aluno *)p2).notaFinal = @(1);
    
    teste(a1);
    
    return 0;
}




