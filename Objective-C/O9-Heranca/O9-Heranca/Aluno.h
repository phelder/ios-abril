//
//  Aluno.h
//  O9-Heranca
//
//  Created by Helder Pereira on 23/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@interface Aluno : Pessoa

@property (strong, nonatomic) NSString *turma;
@property (strong, nonatomic) NSNumber *notaFinal;

@end
