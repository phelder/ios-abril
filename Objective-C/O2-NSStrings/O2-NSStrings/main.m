//
//  main.m
//  O2-NSStrings
//
//  Created by Helder Pereira on 19/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    NSString *texto1 = @"Um texto qualquer";
    
    // texto1.length();
    int tamanho1 = (int)[texto1 length];
    
    NSLog(@"TEXTO: %@", texto1);
    NSLog(@"TAM: %d", tamanho1);    
    
    return 0;
}
