//
//  Pessoa.m
//  O3-ClassesEObjectos
//
//  Created by Helder Pereira on 19/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@implementation Pessoa
{
    NSString *_nome;
    int _idade;
    NSString *_cidade;
}

// String nome() { ... }
- (NSString *)nome {
    return _nome;
}

// void setNome(String nome) { ... }
- (void)setNome:(NSString *)nome {
    
    NSLog(@"OLA %@", nome);
    
    _nome = nome;
}

- (int)idade {
    return _idade;
}

- (void)setIdade:(int)idade {
    _idade = idade;
}

- (NSString *)cidade {
    return _cidade;
}

- (void)setCidade:(NSString *)cidade {
    _cidade = cidade;
}


@end
