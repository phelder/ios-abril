//
//  main.m
//  O3-ClassesEObjectos
//
//  Created by Helder Pereira on 19/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
    
    //Pessoa p1 = new Pessoa();
    //Pessoa *p1 = [Pessoa new];
    
    Pessoa *p1 = [[Pessoa alloc] init];
    
    [p1 setNome:@"Tobias"];
    [p1 setIdade:40];
    [p1 setCidade:@"Porto"];
    
    Pessoa *p2 = [[Pessoa alloc] init];
    p2.nome = @"Tomé"; //[p2 setNome:@"Tomé"];
    p2.idade = 30;
    p2.cidade = @"Aveiro";
    
    Pessoa *maisVelho = nil;
    
    if (p1.idade > p2.idade) {
        maisVelho = p1;
    } else {
        maisVelho = p2;
    }
    
    NSLog(@"Mais velho é o %@ de %@ com %d anos", [maisVelho nome], maisVelho.cidade, maisVelho.idade);
    
    return 0;
    
}