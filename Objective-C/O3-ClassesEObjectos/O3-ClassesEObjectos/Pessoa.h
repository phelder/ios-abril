//
//  Pessoa.h
//  O3-ClassesEObjectos
//
//  Created by Helder Pereira on 19/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pessoa : NSObject

- (NSString *)nome;
- (void)setNome:(NSString *)nome;

- (int)idade;
- (void)setIdade:(int)idade;

- (NSString *)cidade;
- (void)setCidade:(NSString *)cidade;

@end
