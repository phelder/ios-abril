//
//  main.m
//  O22-ARC
//
//  Created by Helder Pereira on 28/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UMObjecto.h"

int main(int argc, const char * argv[]) {
    
    // O ARC ESTA SEMPRE LIGADO POR DEFEITO
    // E DEVE SEMPRE ESTAR...
    
    UMObjecto *umo1 = [[UMObjecto alloc] init];
    UMObjecto *umo2 = umo1;
    
    umo1 = [[UMObjecto alloc] init];
    
    // O OBJECTIVO DO SCANF E FAZER UMA
    // PAUSE NA APP DE TERMINAL
    int a;
    scanf("%d", &a);
    
    NSLog(@"FIM");
    
    return 0;
}
