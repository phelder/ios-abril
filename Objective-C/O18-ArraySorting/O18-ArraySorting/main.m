//
//  main.m
//  O18-ArraySorting
//
//  Created by Helder Pereira on 26/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
    
    NSArray *numeros = @[
                         @"4",
                         @"23",
                         @"54",
                         @"10",
                         @"54",
                         @"20",
                         @"900"
    ];
    
    NSArray *ordenados = [numeros sortedArrayUsingSelector:@selector(compare:)];
    
    NSLog(@"%@", ordenados);
    
//    NSString *n1 = @"Marc";
//    NSString *n2 = @"Marc";
//    
//    if ([n1 compare:n2] == NSOrderedSame) {
//        NSLog(@"YAY");
//    } else {
//        NSLog(@"BOOO");
//    }
    
    
    NSArray *gente = @[
   [Pessoa pessoaWithNome:@"A" idade:@(34) cidade:@"Porto"],
   [Pessoa pessoaWithNome:@"B" idade:@(12) cidade:@"Porto"],
   [Pessoa pessoaWithNome:@"C" idade:@(34) cidade:@"Lisboa"],
   [Pessoa pessoaWithNome:@"D" idade:@(54) cidade:@"Lisboa"],
   [Pessoa pessoaWithNome:@"E" idade:@(20) cidade:@"Lisboa"],
   [Pessoa pessoaWithNome:@"F" idade:@(19) cidade:@"Porto"],
   [Pessoa pessoaWithNome:@"G" idade:@(18) cidade:@"Lisboa"],
   [Pessoa pessoaWithNome:@"H" idade:@(17) cidade:@"Porto"],
   [Pessoa pessoaWithNome:@"I" idade:@(20) cidade:@"Lisboa"],
   [Pessoa pessoaWithNome:@"J" idade:@(56) cidade:@"Lisboa"]
                       ];
    
    NSArray *genteOrdenada = [gente sortedArrayUsingSelector:@selector(comparaPorCidade:)];
    
    NSLog(@"%@", genteOrdenada);
        
    
    return 0;
}
