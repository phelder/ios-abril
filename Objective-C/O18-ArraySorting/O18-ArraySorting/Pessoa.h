//
//  Pessoa.h
//  O12-MetodosDaClasse
//
//  Created by Helder Pereira on 23/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pessoa : NSObject

@property (strong, nonatomic) NSString *nome;
@property (strong, nonatomic) NSNumber *idade;
@property (strong, nonatomic) NSString *cidade;

// initWithNome:idade:cidade:
- (instancetype)initWithNome:(NSString *)nome idade:(NSNumber *)idade cidade:(NSString *)cidade;

+ (instancetype)pessoaWithNome:(NSString *)nome idade:(NSNumber *)idade cidade:(NSString *)cidade;

- (void)salta;

- (NSComparisonResult)comparaPorNome:(Pessoa *)outraPessoa;
- (NSComparisonResult)comparaPorIdade:(Pessoa *)outraPessoa;
- (NSComparisonResult)comparaPorCidade:(Pessoa *)outraPessoa;


@end
