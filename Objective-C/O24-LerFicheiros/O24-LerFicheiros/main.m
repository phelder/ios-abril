//
//  main.m
//  O24-LerFicheiros
//
//  Created by Helder Pereira on 28/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    NSFileManager *fileMan = [NSFileManager defaultManager];
    
    NSString *desktopPath = [fileMan URLsForDirectory:NSDesktopDirectory inDomains:NSUserDomainMask][0].path;
    
    NSString *filePath = [desktopPath stringByAppendingPathComponent:@"nomes.txt"];
    
    NSString *contents = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    
    
    NSArray *nomes = [contents componentsSeparatedByString:@"\n"];
    
    NSLog(@"%@", nomes);
    
    NSLog(@"Introduza um novo nome:");
    char input[20];
    fgets(input, 20, stdin);
    input[strlen(input) -1] = '\0';

    
    NSString *novoNome = [NSString stringWithCString:input encoding:NSUTF8StringEncoding];
    
    
    contents = [contents stringByAppendingFormat:@"\n%@", novoNome];
    
    
    [contents writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    return 0;
}
