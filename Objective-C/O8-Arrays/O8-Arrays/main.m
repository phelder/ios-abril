//
//  main.m
//  O8-Arrays
//
//  Created by Helder Pereira on 21/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
   
    NSArray *idades = @[
                        @(35),
                        @(36),
                        @(28),
                        @(24),
                        @(24),
                        @(31),
                        @(35)
                        ];
    
    //NSNumber *primeiraIdade = [idades objectAtIndex:0];
    NSNumber *primeiraIdade = idades[0];
    
    int soma = 0;
    int maisVelho = primeiraIdade.intValue;
    
    int maisNovo = ((NSNumber *)idades[0]).intValue;
    
    for (int i = 0; i < idades.count; i++) {
    
        NSNumber *idade = idades[i];
        
        soma += idade.intValue; //soma = soma + idade.intValue;
     
        if (idade.intValue > maisVelho) {
            maisVelho = idade.intValue;
        }
        
        if (idade.intValue < maisNovo) {
            maisNovo = idade.intValue;
        }
        
    }
    
    NSLog(@"Soma: %d", soma);
    NSLog(@"Média: %f", soma / (float)idades.count);
    NSLog(@"Mais novo: %d", maisNovo);
    NSLog(@"Mais velho: %d", maisVelho);
    
    return 0;
}
