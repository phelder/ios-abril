//
//  EuroMilhoes.h
//  O20-EuroMilhoes
//
//  Created by Helder Pereira on 26/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EuroMilhoes : NSObject

@property (strong, nonatomic, readonly) NSArray<NSNumber *> *numeros;
@property (strong, nonatomic, readonly) NSArray<NSNumber *> *estrelas;

- (NSString *)stringNumeros; // EX: 12, 23, 25, 30, 42
- (NSString *)stringEstrelas; // EX: 3, 7

- (NSString *)chave; // EX: 12, 23, 25, 30, 42 - 3, 7

- (NSComparisonResult)compare:(EuroMilhoes *)otherObject;

@end
