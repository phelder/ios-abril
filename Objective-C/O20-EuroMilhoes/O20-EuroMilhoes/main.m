//
//  main.m
//  O20-EuroMilhoes
//
//  Created by Helder Pereira on 26/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EuroMilhoes.h"

int main(int argc, const char * argv[]) {

    EuroMilhoes *em1 = [[EuroMilhoes alloc] init];
    NSLog(@"%@", em1);
    
    
    int contador = 0;
    while (true) {
    
        contador ++;
        EuroMilhoes *em2 = [[EuroMilhoes alloc] init];
        if ([em1 compare:em2] == NSOrderedSame) {
            NSLog(@"ENCONTREI");
            break;
        }
        
        if (contador % 100000 == 0) {
            NSLog(@"%d", contador);
        }
    }
    
    NSLog(@"ENCONTREI: %d", contador);
    
    
    return 0;
}
