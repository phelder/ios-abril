//
//  EuroMilhoes.m
//  O20-EuroMilhoes
//
//  Created by Helder Pereira on 26/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "EuroMilhoes.h"
#define NUM_NUMEROS 5
#define NUM_ESTRELAS 1

#define MIN_NUMEROS 1
#define MAX_NUMEROS 50

#define MIN_ESTRELAS 1
#define MAX_ESTRELAS 11

@implementation EuroMilhoes

+ (int)randomizaEntre:(int)min e:(int)max {
    return (arc4random() % (max - min + 1)) + min;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self geraChave];
    }
    return self;
}

- (void)geraChave {

    NSMutableArray *numeros = [[NSMutableArray alloc] init];
    NSMutableArray *estrelas = [[NSMutableArray alloc] init];
    
    
    // GERA NUMEROS
    for (int i = 0; i < NUM_NUMEROS; i++) {
    
        NSNumber *n = nil;
        
        do {
            n = @([EuroMilhoes randomizaEntre:MIN_NUMEROS e:MAX_NUMEROS]);
        
        } while ([numeros containsObject:n]);
        
        [numeros addObject:n];
    }
    
    // GERA ESTRELAS
    for (int i = 0; i < NUM_ESTRELAS; i++) {
        
        NSNumber *n = nil;
        
        do {
            n = @([EuroMilhoes randomizaEntre:MIN_ESTRELAS e:MAX_ESTRELAS]);
            
        } while ([estrelas containsObject:n]);
        
        [estrelas addObject:n];
    }
    
    // ORDENA E COLOCA NAS PROPERTIES PUBLICAS
    _numeros = [numeros sortedArrayUsingSelector:@selector(compare:)];
    _estrelas = [estrelas sortedArrayUsingSelector:@selector(compare:)];
}

- (NSString *)stringNumeros
{
//    NSMutableString *newString = [[NSMutableString alloc] init];
//    
//    for (int i = 0; i < _numeros.count; i++) {
//        
//        if (i != 0) {
//            [newString appendString:@", "];
//        }
//        int n = _numeros[i].intValue;
//        
//        [newString appendFormat:@"%d", n];
//        
//    }
//    return newString;
    
    return [_numeros componentsJoinedByString:@", "];
}

- (NSString *)stringEstrelas {
    return [_estrelas componentsJoinedByString:@", "];
}

- (NSString *)chave {
    return [NSString stringWithFormat:@"%@ - %@", self.stringNumeros, self.stringEstrelas];
}

- (NSComparisonResult)compare:(EuroMilhoes *)otherObject
{
    return [self.chave compare:otherObject.chave];
}

- (NSString *)description
{
    return self.chave;
}


@end
