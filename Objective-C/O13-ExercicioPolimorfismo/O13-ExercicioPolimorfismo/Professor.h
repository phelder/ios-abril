//
//  Professor.h
//  O13-ExercicioPolimorfismo
//
//  Created by Helder Pereira on 23/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@interface Professor : Pessoa

@property (strong, nonatomic) NSArray<NSString *> *turmas;

- (instancetype)initWithNome:(NSString *)nome idade:(NSNumber *)idade cidade:(NSString *)cidade turmas:(NSArray<NSString *> *)turmas;

+ (instancetype)professorWithNome:(NSString *)nome idade:(NSNumber *)idade cidade:(NSString *)cidade turmas:(NSArray<NSString *> *)turmas;

@end
