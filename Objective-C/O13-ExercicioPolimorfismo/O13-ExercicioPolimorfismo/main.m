//
//  main.m
//  O13-ExercicioPolimorfismo
//
//  Created by Helder Pereira on 23/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Aluno.h"
#import "Professor.h"

int main(int argc, const char * argv[]) {
    
    // 3 CLASSES...
    // Classe Pessoa (nome, idade, cidade);
    // Classe Aluno: Pessoa (turma, nota);
    // Classe Professor: Pessoa (turmas);
    
    // Criem um array com alunos e professores
    // UNICO ARRAY PRE-FEITO
    
    // PERCORRER O ARRAY PRE-FEITO
    // CRIAR 2 novos ARRAYS
    // UM SO COM ALUNOS
    // OUTRO SO COM PROFESSORES
    
    // média de idades dos alunos
    // média de idades dos professores
    // média de idades geral
    // NOME DO ALUNO com a nota mais alta
    

    NSArray<Pessoa *> *gente = @[

             [Professor professorWithNome:@"Batman"
                                    idade:@(36)
                                   cidade:@"Gotham City"
                                   turmas:@[@"A", @"B"]
              ],
             [Professor professorWithNome:@"Superman"
                                    idade:@(32)
                                   cidade:@"Metropolis"
                                   turmas:@[]
              ],
             [Professor professorWithNome:@"Aquaman"
                                    idade:@(40)
                                   cidade:@"Atlantis"
                                   turmas:@[]
              ],
             [Professor professorWithNome:@"Wonder Woman"
                                    idade:@(200)
                                   cidade:@"New Themyscira"
                                   turmas:@[]
              ],
             [Professor professorWithNome:@"Green Lantern"
                                    idade:@(38)
                                   cidade:@"Coast City"
                                   turmas:@[]
              ],
             [Professor professorWithNome:@"Flash"
                                    idade:@(28)
                                   cidade:@"Start City"
                                   turmas:@[]
              ],
             [Aluno alunoWithNome:@"Robin"
                            idade:@(14)
                           cidade:@"Gotham City"
                            turma:@"A"
                        notaFinal:@(20)],
             [Aluno alunoWithNome:@"Kid Flash"
                            idade:@(15)
                           cidade:@"Star City"
                            turma:@"A"
                        notaFinal:@(15)
              ],
             [Aluno alunoWithNome:@"Aqualad"
                            idade:@(17)
                           cidade:@"Atlantis"
                            turma:@"A"
                        notaFinal:@(17)
              ],
             [Aluno alunoWithNome:@"Darkstar"
                            idade:@(16)
                           cidade:@"Themyscira"
                            turma:@"A"
                        notaFinal:@(12)
              ],
             [Aluno alunoWithNome:@"Arsenal"
                            idade:@(15)
                           cidade:@"Star City"
                            turma:@"A"
                        notaFinal:@(10)
              ]
                                 ];
    NSMutableArray<Professor *> *profs = [[NSMutableArray alloc] init];
    NSMutableArray<Aluno *> *alunos = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < gente.count; i++) {
        
        Pessoa *p = gente[i];
        
        if ([p isKindOfClass:[Professor class]]) {
            [profs addObject:(Professor *)p];
            
        } else if ([p isKindOfClass:[Aluno class]]) {
            
            [alunos addObject:(Aluno *)p];
        }
    }
    
    int somaProfs = 0;
    for (int i = 0; i < profs.count; i++) {
        somaProfs += profs[i].idade.intValue;
    }
    
    
    Aluno *melhorAluno = alunos[0];
    
    int somaAlunos = 0;
    for (int i = 0; i < alunos.count; i++) {
        somaAlunos += alunos[i].idade.intValue;
        
        if (alunos[i].notaFinal.intValue > melhorAluno.notaFinal.intValue) {
            melhorAluno = alunos[i];
        }
    }
    
    float mediaProfs = somaProfs / (float)profs.count;
    float mediaAlunos = somaAlunos / (float)alunos.count;
    float mediaGeral = (mediaProfs + mediaAlunos) / 2.0;
    
    NSLog(@"SOMA IDADES ALUNOS: %d", somaAlunos);
    NSLog(@"MEDIA IDADES ALUNOS: %f", mediaAlunos);
    
    NSLog(@"SOMA IDADES PROFESSORES: %d", somaProfs);
    NSLog(@"MEDIA IDADES PROFESSORES: %f", mediaProfs);
    
    NSLog(@"MEDIA GERAL: %f", mediaGeral);

    NSLog(@"MELHOR ALUNO: %@", melhorAluno);
    
    return 0;
}
