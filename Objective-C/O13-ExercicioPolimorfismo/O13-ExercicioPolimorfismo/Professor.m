//
//  Professor.m
//  O13-ExercicioPolimorfismo
//
//  Created by Helder Pereira on 23/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Professor.h"

@implementation Professor

- (instancetype)initWithNome:(NSString *)nome idade:(NSNumber *)idade cidade:(NSString *)cidade turmas:(NSArray<NSString *> *)turmas {
    
    self = [super initWithNome:nome idade:idade cidade:cidade];
    if (self) {
        _turmas = turmas;
    }
    return self;
}

+ (instancetype)professorWithNome:(NSString *)nome idade:(NSNumber *)idade cidade:(NSString *)cidade turmas:(NSArray<NSString *> *)turmas {
    
    return [[Professor alloc] initWithNome:nome idade:idade cidade:cidade turmas:turmas];
}


@end
