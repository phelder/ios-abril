//
//  Aluno.m
//  O12-MetodosDaClasse
//
//  Created by Helder Pereira on 23/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Aluno.h"

@implementation Aluno

- (instancetype)initWithNome:(NSString *)nome idade:(NSNumber *)idade cidade:(NSString *)cidade turma:(NSString *)turma notaFinal:(NSNumber *)notaFinal
{
//    self = [super init];
//    if (self) {
//        self.nome = nome;
//        self.idade = idade;
//        self.cidade = cidade;
//        _turma = turma;
//        _notaFinal = notaFinal;
//    }
//    return self;
    self = [super initWithNome:nome idade:idade cidade:cidade];
    if (self) {
        _turma = turma;
        _notaFinal = notaFinal;
    }
    return self;
}

+ (instancetype)alunoWithNome:(NSString *)nome idade:(NSNumber *)idade cidade:(NSString *)cidade turma:(NSString *)turma notaFinal:(NSNumber *)notaFinal
{
    return [[Aluno alloc] initWithNome:nome idade:idade cidade:cidade turma:turma notaFinal:notaFinal];
}

@end
