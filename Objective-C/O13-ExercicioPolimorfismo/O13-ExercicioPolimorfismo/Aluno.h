//
//  Aluno.h
//  O12-MetodosDaClasse
//
//  Created by Helder Pereira on 23/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@interface Aluno : Pessoa

@property (strong, nonatomic) NSString *turma;
@property (strong, nonatomic) NSNumber *notaFinal;

- (instancetype)initWithNome:(NSString *)nome idade:(NSNumber *)idade cidade:(NSString *)cidade turma:(NSString *)turma notaFinal:(NSNumber *)notaFinal;

+ (instancetype)alunoWithNome:(NSString *)nome idade:(NSNumber *)idade cidade:(NSString *)cidade turma:(NSString *)turma notaFinal:(NSNumber *)notaFinal;

@end
