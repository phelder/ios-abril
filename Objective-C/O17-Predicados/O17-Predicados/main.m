//
//  main.m
//  O17-Predicados
//
//  Created by Helder Pereira on 23/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {

    NSArray *nomes = @[
                       @"Helder",
                       @"Marc",
                       @"Luís",
                       @"Ricardo",
                       @"Rui",
                       @"Vanessa",
                       @"Ricardo Remoto"
                       ];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self CONTAINS [cd]%@", @"I"];
    
    NSArray *filtrado = [nomes filteredArrayUsingPredicate:predicate];
    
    NSLog(@"%@", filtrado);
    
    // CRIAR ARRAY COM 5 OBJECTOS CLASSE PESSOA
    // FILTRAR PARA UM NOVO ARRAY APENAS OS QUE TEM
    // MAIS DE 18 ANOS E SAO DO PORTO
    
    NSArray *gente = @[
[Pessoa pessoaWithNome:@"A" idade:@(34) cidade:@"Porto"],
[Pessoa pessoaWithNome:@"B" idade:@(12) cidade:@"Porto"],
[Pessoa pessoaWithNome:@"C" idade:@(34) cidade:@"Lisboa"],
[Pessoa pessoaWithNome:@"D" idade:@(54) cidade:@"Lisboa"],
[Pessoa pessoaWithNome:@"E" idade:@(20) cidade:@"Lisboa"],
[Pessoa pessoaWithNome:@"F" idade:@(19) cidade:@"Porto"],
[Pessoa pessoaWithNome:@"G" idade:@(18) cidade:@"Lisboa"],
[Pessoa pessoaWithNome:@"H" idade:@(17) cidade:@"Porto"],
[Pessoa pessoaWithNome:@"I" idade:@(20) cidade:@"Lisboa"],
[Pessoa pessoaWithNome:@"J" idade:@(56) cidade:@"Lisboa"]
                       ];
    //NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"idade >= 18 AND cidade CONTAINS [cd]'porto'"];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"idade >= %d AND cidade CONTAINS [cd]%@", 18, @"porto"];
    
    
    NSArray *genteFiltrada = [gente filteredArrayUsingPredicate:predicate2];
    
    NSLog(@"%@", genteFiltrada);
    
    return 0;
}
