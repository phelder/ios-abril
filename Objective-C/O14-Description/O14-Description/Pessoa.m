//
//  Pessoa.m
//  O12-MetodosDaClasse
//
//  Created by Helder Pereira on 23/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@implementation Pessoa

- (instancetype)initWithNome:(NSString *)nome idade:(NSNumber *)idade cidade:(NSString *)cidade {
    
    self = [super init];
    if (self) {
        _nome = nome;
        _idade = idade;
        _cidade = cidade;
    }
    return self;
}

+ (instancetype)pessoaWithNome:(NSString *)nome idade:(NSNumber *)idade cidade:(NSString *)cidade {

    return [[Pessoa alloc] initWithNome:nome idade:idade cidade:cidade];
}

- (void)salta {
    
    NSLog(@"O(A) %@ saltou!!!", _nome);
    
    
}

- (NSString *)description
{
    
//    return [NSString stringWithFormat:@"%@: %@ - %@ - %@", super.description, _nome, _idade, _cidade];
    
    
    NSString *descricaoOriginal = super.description;
    NSString *descricaoCustom = [NSString stringWithFormat:@"%@ - %@ - %@", _nome, _idade, _cidade];
    
    NSString *novaDescricao = [NSString stringWithFormat:@"%@:%@", descricaoOriginal, descricaoCustom];
    
    return novaDescricao;
}

@end
