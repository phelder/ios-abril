//
//  main.m
//  O14-Description
//
//  Created by Helder Pereira on 23/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
    
    NSString *nome = @"Teste";
    NSNumber *num = @(5678);
    NSArray *cenas = @[@"Texto", @"outro texto", @(23), @(56)];
    
    Pessoa *p1 = [Pessoa pessoaWithNome:@"Tobias" idade:@(10) cidade:@"Porto"];
    
    NSLog(@"%@", nome);
    NSLog(@"%@", num);
    NSLog(@"%@", cenas);
    NSLog(@"%@", p1);    
    
    return 0;
}
