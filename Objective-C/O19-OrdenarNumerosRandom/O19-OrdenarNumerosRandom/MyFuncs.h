//
//  MyFuncs.h
//  O19-OrdenarNumerosRandom
//
//  Created by Helder Pereira on 26/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyFuncs : NSObject

+ (int)randomizaEntre:(int)min e:(int)max;

@end
