//
//  main.m
//  O19-OrdenarNumerosRandom
//
//  Created by Helder Pereira on 26/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyFuncs.h"

int main(int argc, const char * argv[]) {
    
    
    NSMutableArray *numerosRandom = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < 20; i++) {
        
        int n = [MyFuncs randomizaEntre:1 e:100];
        
        [numerosRandom addObject:@(n)];
    }
    
    [numerosRandom sortedArrayUsingSelector:@selector(compare:)];
    
    NSLog(@"%@", numerosRandom);
    
    
    return 0;
}
