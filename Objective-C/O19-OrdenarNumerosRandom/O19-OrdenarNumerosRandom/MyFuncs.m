//
//  MyFuncs.m
//  O19-OrdenarNumerosRandom
//
//  Created by Helder Pereira on 26/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "MyFuncs.h"

@implementation MyFuncs

+ (int)randomizaEntre:(int)min e:(int)max {
    return (arc4random() % (max - min + 1)) + min;
}

@end
