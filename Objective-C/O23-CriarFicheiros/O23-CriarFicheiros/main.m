//
//  main.m
//  O23-CriarFicheiros
//
//  Created by Helder Pereira on 28/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    //[@"Um texto qualquer" writeToFile:@"/Users/Helder/Desktop/exemplo1.txt" atomically:YES encoding:NSUTF8StringEncoding error:nil];

    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray<NSURL *> *urls = [fileManager URLsForDirectory:NSDesktopDirectory inDomains:NSUserDomainMask];
    
    NSURL *desktopURL = urls[0];

    NSString *desktopPath = desktopURL.path;
    
    NSString *filePath = [desktopPath stringByAppendingPathComponent:@"exemplo2.txt"];
    
    
    if ([fileManager fileExistsAtPath:filePath] == NO) {
        NSString *fileContent = @"EU VOU ESTAR NUM FICHEIRO\n\n\nYAY!!!!!\n\t\t\tWEEEE!!!!\n\n\nFIM";
        
        BOOL success = [fileContent writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
        
        if (success) {
            NSLog(@"FICHEIRO CRIADO COM SUCESSO");
        } else {
            NSLog(@"FICHEIRO NAO CRIADO");
        }
    } else {
        NSLog(@"FICHEIro JA EXISTE");
    }
    
    
    
    
    return 0;
}
