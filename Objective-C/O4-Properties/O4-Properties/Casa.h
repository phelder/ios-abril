//
//  Casa.h
//  O4-Properties
//
//  Created by Helder Pereira on 19/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Casa : NSObject

@property (strong, nonatomic) NSString *morada;
@property (strong, nonatomic) NSString *cor;

@end
