//
//  Carro.h
//  O4-Properties
//
//  Created by Helder Pereira on 19/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Carro : NSObject

@property (strong, nonatomic) NSString *matricula;
@property (strong, nonatomic) NSString *marca;
@property (strong, nonatomic) NSString *modelo;
@property (strong, nonatomic) NSString *cor;

@end
