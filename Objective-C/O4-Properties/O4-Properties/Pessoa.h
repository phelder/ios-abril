//
//  Pessoa.h
//  O4-Properties
//
//  Created by Helder Pereira on 19/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Casa.h"

@interface Pessoa : NSObject

@property NSString *nome;
@property int idade;
@property NSString *cidade;
@property Casa *casa;

@end
