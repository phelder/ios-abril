//
//  main.m
//  O4-Properties
//
//  Created by Helder Pereira on 19/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"
#import "Carro.h"

int main(int argc, const char * argv[]) {
    
    Pessoa *p1 = [[Pessoa alloc] init];
    p1.nome = @"Helder";
    p1.idade = 35;
    p1.cidade = @"Porto";
    
    Casa *c1 = [[Casa alloc] init];
    c1.morada = @"A minha morada...";
    c1.cor = @"Amarela";
    
    p1.casa = c1;    
    
    return 0;
}
