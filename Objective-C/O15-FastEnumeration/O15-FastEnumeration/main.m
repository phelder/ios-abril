//
//  main.m
//  O15-FastEnumeration
//
//  Created by Helder Pereira on 23/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    NSArray *numeros = @[ @(23), @(34), @(78) ];
    
    for (int i = 0; i < numeros.count; i++) {
        NSNumber *num = numeros[i];
        
        // FAÇO O QUE ME APETECER...
    }
    
    for (NSNumber *num in numeros) {
        NSLog(@"%@", num);
    }
    
    return 0;
}
