//
//  TabelaCidadesDelegate.m
//  I7-DualTableInfo
//
//  Created by Helder Pereira on 05/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "TabelaCidadesDelegate.h"

@implementation TabelaCidadesDelegate

- (instancetype)initWithCidades:(NSArray<Cidade *> *)cidades
{
    self = [super init];
    if (self) {
        _cidades = cidades;
    }
    return self;
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _cidades.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell"];
    
    cell.textLabel.text = _cidades[indexPath.row].nome;
    
    return cell;
}

#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"TESTE");
    [self.handler actualizaGente:indexPath];
}

@end
