//
//  Cidade.h
//  I7-DualTableInfo
//
//  Created by Helder Pereira on 05/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Cidade : NSObject

@property (strong, nonnull) NSString *nome;
@property (strong, nonnull) NSNumber *area;

- (instancetype)initWithNome:(NSString *)nome area:(NSNumber *)area;

+ (instancetype)cidadeWithNome:(NSString *)nome area:(NSNumber *)area;


@end
