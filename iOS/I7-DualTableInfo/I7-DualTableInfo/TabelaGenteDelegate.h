//
//  TabelaGenteDelegate.h
//  I7-DualTableInfo
//
//  Created by Helder Pereira on 05/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Pessoa.h"

@interface TabelaGenteDelegate : NSObject <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSArray <Pessoa *> *gente;

@end
