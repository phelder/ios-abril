//
//  ViewController.m
//  I7-DualTableInfo
//
//  Created by Helder Pereira on 05/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"
#import "Cidade.h"
#import "Pessoa.h"
#import "TabelaCidadesDelegate.h"
#import "TabelaGenteDelegate.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableViewCidades;
@property (weak, nonatomic) IBOutlet UITableView *tableViewGente;

@end

@implementation ViewController
{
    NSArray<Cidade *> *_cidades;
    NSArray<Pessoa *> *_gente;
    
    TabelaCidadesDelegate *_tcd;
    TabelaGenteDelegate *_tgd;
}

- (void)criarDados
{
    // DUMMY DATA
    _cidades = @[
              [Cidade cidadeWithNome:@"Porto" area:@(1)],
              [Cidade cidadeWithNome:@"Lisboa" area:@(2)],
              [Cidade cidadeWithNome:@"Aveiro" area:@(3)]
              ];
    
    _gente = @[
               [Pessoa pessoaWithNome:@"A" idade:@(1) cidade:_cidades[0]],
               [Pessoa pessoaWithNome:@"B" idade:@(2) cidade:_cidades[0]],
               [Pessoa pessoaWithNome:@"C" idade:@(3) cidade:_cidades[1]],
               [Pessoa pessoaWithNome:@"D" idade:@(4) cidade:_cidades[1]],
               [Pessoa pessoaWithNome:@"E" idade:@(5) cidade:_cidades[2]],
               [Pessoa pessoaWithNome:@"F" idade:@(6) cidade:_cidades[2]],
               ];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self criarDados];

    _tcd = [[TabelaCidadesDelegate alloc] initWithCidades:_cidades];
    
    _tcd.handler = self;
    
    self.tableViewCidades.dataSource = _tcd;
    self.tableViewCidades.delegate = _tcd;
    
}

- (void)actualizaGente:(NSIndexPath *)indexPath;
{
 
    Cidade *cidadeEscolhida = _cidades[indexPath.row];
    NSLog(@"%@", cidadeEscolhida.nome);
    
    NSMutableArray *genteEscolhida = [[NSMutableArray alloc] init];
    for (int i = 0; i < _gente.count; i++) {
        if (cidadeEscolhida == _gente[i].cidade) {
            [genteEscolhida addObject:_gente[i]];
        }
    }
    
    NSLog(@"%@", genteEscolhida);
    
    _tgd = [[TabelaGenteDelegate alloc] init];
    _tgd.gente = genteEscolhida;
    
    self.tableViewGente.dataSource = _tgd;
    self.tableViewGente.delegate = _tgd;
    
    [self.tableViewGente reloadData];
    
}




@end
