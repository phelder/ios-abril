//
//  Pessoa.m
//  I7-DualTableInfo
//
//  Created by Helder Pereira on 05/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@implementation Pessoa

- (instancetype)initWithNome:(NSString *)nome idade:(NSNumber *)idade cidade:(Cidade *)cidade
{
    self = [super init];
    if (self) {
        _nome = nome;
        _idade = idade;
        _cidade = cidade;
    }
    return self;
}

+ (instancetype)pessoaWithNome:(NSString *)nome idade:(NSNumber *)idade cidade:(Cidade *)cidade
{
    return [[Pessoa alloc] initWithNome:nome idade:idade cidade:cidade];
}

@end
