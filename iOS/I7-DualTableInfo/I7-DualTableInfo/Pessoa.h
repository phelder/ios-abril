//
//  Pessoa.h
//  I7-DualTableInfo
//
//  Created by Helder Pereira on 05/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Cidade;

@interface Pessoa : NSObject

@property (strong, nonatomic) NSString *nome;
@property (strong, nonatomic) NSNumber *idade;
@property (strong, nonatomic) Cidade *cidade;

- (instancetype)initWithNome:(NSString *)nome idade:(NSNumber *)idade cidade:(Cidade *)cidade;

+ (instancetype)pessoaWithNome:(NSString *)nome idade:(NSNumber *)idade cidade:(Cidade *)cidade;

@end
