//
//  Cidade.m
//  I7-DualTableInfo
//
//  Created by Helder Pereira on 05/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Cidade.h"

@implementation Cidade

- (instancetype)initWithNome:(NSString *)nome area:(NSNumber *)area
{
    self = [super init];
    if (self) {
        _nome = nome;
        _area = area;
    }
    return self;
}

 + (instancetype)cidadeWithNome:(NSString *)nome area:(NSNumber *)area
{
    return [[Cidade alloc] initWithNome:nome area:area];
}

@end
