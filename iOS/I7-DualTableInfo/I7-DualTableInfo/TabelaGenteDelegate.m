//
//  TabelaGenteDelegate.m
//  I7-DualTableInfo
//
//  Created by Helder Pereira on 05/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "TabelaGenteDelegate.h"

@implementation TabelaGenteDelegate

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _gente.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell"];
    
    cell.textLabel.text = _gente[indexPath.row].nome;
    
    return cell;
}

@end
