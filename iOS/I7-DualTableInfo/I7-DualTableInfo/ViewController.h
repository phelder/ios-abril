//
//  ViewController.h
//  I7-DualTableInfo
//
//  Created by Helder Pereira on 05/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (void)actualizaGente:(NSIndexPath *)indexPath;

@end

