//
//  TabelaCidadesDelegate.h
//  I7-DualTableInfo
//
//  Created by Helder Pereira on 05/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Cidade.h"
#import "ViewController.h"

@interface TabelaCidadesDelegate : NSObject <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSArray<Cidade *> *cidades;

@property (strong, nonatomic) ViewController *handler;

- (instancetype)initWithCidades:(NSArray<Cidade *> *)cidades;

@end
