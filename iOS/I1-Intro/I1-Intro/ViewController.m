//
//  ViewController.m
//  I1-Intro
//
//  Created by Helder Pereira on 30/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *labelHelloWorld;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    NSLog(@"OLA");
    
    self.view.backgroundColor = [UIColor greenColor];

    self.labelHelloWorld.text = @"Teste";
}

- (IBAction)clickedButton:(id)sender
{
    
    self.labelHelloWorld.text = @"Mudou o texto";
    
}

@end
