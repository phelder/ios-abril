//
//  AppDelegate.h
//  I5-IntroTabelas_Explicacao
//
//  Created by Helder Pereira on 03/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

