//
//  ViewController.m
//  I5-IntroTabelas_Explicacao
//
//  Created by Helder Pereira on 03/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView1;

@end

@implementation ViewController
{
    NSArray <NSString *> *_nomes;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView1.dataSource = self;
    self.tableView1.delegate = self;
    
    _nomes = @[
               @"Ricardo Cardoso",
               @"Marc Martins",
               @"Helder Pereira",
               @"Ricardo Silva",
               @"Luís Peixoto",
               @"Vanessa Rodrigues",
               @"Rui Cruz",
               @"TOBIAS",
               @"Ricardo Cardoso",
               @"Marc Martins",
               @"Helder Pereira",
               @"Ricardo Silva",
               @"Luís Peixoto",
               @"Vanessa Rodrigues",
               @"Rui Cruz",
               @"TOBIAS",
               @"Ricardo Cardoso",
               @"Marc Martins",
               @"Helder Pereira",
               @"Ricardo Silva",
               @"Luís Peixoto",
               @"Vanessa Rodrigues",
               @"Rui Cruz",
               @"TOBIAS",
               @"Ricardo Cardoso",
               @"Marc Martins",
               @"Helder Pereira",
               @"Ricardo Silva",
               @"Luís Peixoto",
               @"Vanessa Rodrigues",
               @"Rui Cruz",
               @"TOBIAS",
               @"Ricardo Cardoso",
               @"Marc Martins",
               @"Helder Pereira",
               @"Ricardo Silva",
               @"Luís Peixoto",
               @"Vanessa Rodrigues",
               @"Rui Cruz",
               @"TOBIAS",
               @"Ricardo Cardoso",
               @"Marc Martins",
               @"Helder Pereira",
               @"Ricardo Silva",
               @"Luís Peixoto",
               @"Vanessa Rodrigues",
               @"Rui Cruz",
               @"TOBIAS",
               @"Ricardo Cardoso",
               @"Marc Martins",
               @"Helder Pereira",
               @"Ricardo Silva",
               @"Luís Peixoto",
               @"Vanessa Rodrigues",
               @"Rui Cruz",
               @"TOBIAS"
               ];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _nomes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"recycleCell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"recycleCell"];
    
        NSLog(@"CELULA NOVA");

    } else {
        NSLog(@"CELULA RECICLADA");
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"%ld: %@", (long)indexPath.row, _nomes[indexPath.row]];
    
    
    
    return cell;
}

@end
