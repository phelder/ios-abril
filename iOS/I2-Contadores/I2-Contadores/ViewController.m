//
//  ViewController.m
//  I2-Contadores
//
//  Created by Helder Pereira on 07/03/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *labelContador1;
@property (weak, nonatomic) IBOutlet UILabel *labelContador2;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentEscolheContador;

@end

@implementation ViewController
{
    int _contador1;
    int _contador2;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self actualizaContadores];
}

- (IBAction)clickedSobe:(id)sender
{
    if (self.segmentEscolheContador.selectedSegmentIndex == 0) {
        _contador1++;
    } else {
        _contador2++;
    }
    [self actualizaContadores];
}

- (IBAction)clickedDesce:(id)sender {
    if (self.segmentEscolheContador.selectedSegmentIndex == 0) {
        _contador1--;
    } else {
        _contador2--;
    }
    [self actualizaContadores];
}

- (void)actualizaContadores
{
    self.labelContador1.text = [NSString stringWithFormat:@"Contador1: %d", _contador1];
    self.labelContador2.text = [NSString stringWithFormat:@"Contador2: %d", _contador2];
}

@end
