//
//  ViewController.m
//  I19-AFNetworkingSourceCode
//
//  Created by Helder Pereira on 14/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"
#import "AFNetworking.h"
#import "User.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:@"http://jsonplaceholder.typicode.com/users"
      parameters:nil
        progress:nil
         success:^(NSURLSessionDataTask *task, id responseObject) {
      
//             NSLog(@"SUCCESS");
//             NSLog(@"%@", responseObject);
             
             [self parseUserInfo:responseObject];
             
         }
         failure:^(NSURLSessionDataTask *task, NSError *error) {
         
             NSLog(@"ERROR");
             NSLog(@"%@", error);
         
         }];
     
}

- (void)parseUserInfo:(NSArray *)response {

    NSMutableArray *users = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < response.count; i++) {
        
        User *user = [[User alloc] init];
        
        user.webId = response[i][@"id"];
        user.name = response[i][@"name"];
        user.username = response[i][@"username"];
        // TODO... outros campos
        
        user.address = [[UserAddress alloc] init];
        user.address.street = response[i][@"address"][@"street"];
        user.address.suite = response[i][@"address"][@"suite"];
        user.address.city = response[i][@"address"][@"city"];
        user.address.zipcode = response[i][@"address"][@"zipcode"];
        
        NSNumber *lat = response[i][@"address"][@"geo"][@"lat"];
        NSNumber *lng = response[i][@"address"][@"geo"][@"lng"];
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:lat.doubleValue longitude:lng.doubleValue];
        
        user.address.geo = location;
        
        [users addObject:user];
    }
    
    //NSLog(@"%@", users);
    
}


@end
