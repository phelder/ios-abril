//
//  AppDelegate.h
//  I19-AFNetworkingSourceCode
//
//  Created by Helder Pereira on 14/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

