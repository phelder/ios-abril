//
//  Company.h
//  I19-AFNetworkingSourceCode
//
//  Created by Helder Pereira on 14/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Company : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *catchPhrase;
@property (strong, nonatomic) NSString *bs;

@end
