//
//  UserAddress.h
//  I19-AFNetworkingSourceCode
//
//  Created by Helder Pereira on 14/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface UserAddress : NSObject

@property (strong, nonatomic) NSString *street;
@property (strong, nonatomic) NSString *suite;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *zipcode;
@property (strong, nonatomic) CLLocation *geo;


@end
