//
//  User.m
//  I19-AFNetworkingSourceCode
//
//  Created by Helder Pereira on 14/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "User.h"

@implementation User

- (NSString *)description {

    return [NSString stringWithFormat:@"%@ - %@", self.webId, self.address.geo];

}

@end
