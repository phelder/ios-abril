//
//  ViewController.m
//  I17-UserDefaults
//
//  Created by Helder Pereira on 12/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextField *textField1;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self readSomethingFromDefaults];
}

- (IBAction)clickedGuardar:(id)sender {
    
    [self writeSomethingToDefaults];
    
}

- (void)writeSomethingToDefaults {
    // PARA ESCREVER:::
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:self.textField1.text forKey:@"nome"];
   
    [defaults synchronize];
}

- (void)readSomethingFromDefaults {
    // PARA LER:::
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    self.textField1.text = [defaults objectForKey:@"nome"];
    
}

@end
