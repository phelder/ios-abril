//
//  DetailViewController.m
//  Ex-BlocoNotas
//
//  Created by Helder Pereira on 21/03/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "DetailViewController.h"
#import "NotasDataSource.h"

@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet UITextField *textFieldTitulo;
@property (weak, nonatomic) IBOutlet UITextView *textViewConteudo;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.textViewConteudo.layer.borderWidth = 0.3f;
    self.textViewConteudo.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.textViewConteudo.layer.cornerRadius = 5.0f;
    
    if (_nota) {
        self.textFieldTitulo.text = _nota.titulo;
        self.textViewConteudo.text = _nota.conteudo;
    }
}


#pragma mark - UIButtonActions

- (IBAction)clickedGuardar:(id)sender {

    NotasDataSource *ds = [NotasDataSource sharedDataSource];
    
    NSString *titulo = self.textFieldTitulo.text;
    NSString *conteudo = self.textViewConteudo.text;
    
    if (_nota == nil) {
        _nota = [Nota notaWithTitulo:titulo conteudo:conteudo];
        
        [ds.notas addObject:_nota];
    } else {
        _nota.titulo = titulo;
        _nota.conteudo = conteudo;
    }

    [self.navigationController popViewControllerAnimated:YES];
}

@end
