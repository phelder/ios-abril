//
//  DetailViewController.h
//  Ex-BlocoNotas
//
//  Created by Helder Pereira on 21/03/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Nota;

@interface DetailViewController : UIViewController

@property (strong, nonatomic) Nota* nota;

@end
