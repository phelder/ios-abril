//
//  NotasDataSource.m
//  Ex-BlocoNotas
//
//  Created by Helder Pereira on 21/03/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "NotasDataSource.h"

@implementation NotasDataSource

+ (instancetype)sharedDataSource
{
    static NotasDataSource *instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[NotasDataSource alloc] init];
        instance->_notas = [[NSMutableArray alloc] init];
    });
    return instance;
}

@end
