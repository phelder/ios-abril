//
//  Nota.h
//  Ex-BlocoNotas
//
//  Created by Helder Pereira on 21/03/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Nota : NSObject

@property (strong, nonatomic) NSString *titulo;
@property (strong, nonatomic) NSString *conteudo;

- (instancetype)initWithTitulo:(NSString *)titulo conteudo:(NSString *)conteudo;
+ (instancetype)notaWithTitulo:(NSString *)titulo conteudo:(NSString *)conteudo;

@end
