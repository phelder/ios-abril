//
//  MainViewController.m
//  Ex-BlocoNotas
//
//  Created by Helder Pereira on 21/03/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "MainViewController.h"
#import "NotasDataSource.h"
#import "DetailViewController.h"

@interface MainViewController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableViewNotas;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBarNotas;

@end

@implementation MainViewController
{
    NSArray<Nota *> *_filteredNotas;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NotasDataSource *ds = [NotasDataSource sharedDataSource];
    
    [ds.notas addObject:[Nota notaWithTitulo:@"titulo da primeira nota" conteudo:@"cont"]];
    [ds.notas addObject:[Nota notaWithTitulo:@"titulo da 2" conteudo:@"cont2"]];
    
    NSLog(@"%@", ds.notas);
}

- (void)viewWillAppear:(BOOL)animated
{
    [self filter];
    [self.tableViewNotas reloadData];
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _filteredNotas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell"];
    
    Nota *nota = _filteredNotas[indexPath.row];
    
    cell.textLabel.text = nota.titulo;
    
    return cell;
}

#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"MainToDetail" sender:indexPath];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    Nota *nota = _filteredNotas[indexPath.row];
    
    NotasDataSource *ds = [NotasDataSource sharedDataSource];
    [ds.notas removeObject:nota];
    
    [self filter];
    
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - UIButton Actions

- (IBAction)clickedAdd:(id)sender {
    [self performSegueWithIdentifier:@"MainToDetail" sender:nil];
}

#pragma mark - UISearchBarDelegate Methods

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self filter];
    
    [self.tableViewNotas reloadData];
}

- (void)filter
{
    NSLog(@"%@", self.searchBarNotas.text);
    
    NotasDataSource *ds = [NotasDataSource sharedDataSource];
        
    if (self.searchBarNotas.text.length == 0) {
        
        _filteredNotas = [NSMutableArray arrayWithArray:ds.notas];
        
    } else {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"titulo CONTAINS [cd]%@", self.searchBarNotas.text];
        
        _filteredNotas = [ds.notas filteredArrayUsingPredicate:predicate];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if (sender != nil) {
        NSIndexPath *indexPath = sender;
        
        Nota *nota = _filteredNotas[indexPath.row];
        
        DetailViewController *dvc = segue.destinationViewController;
        
        dvc.nota = nota;
    }
}

@end
