//
//  Nota.m
//  Ex-BlocoNotas
//
//  Created by Helder Pereira on 21/03/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Nota.h"

@implementation Nota

- (instancetype)initWithTitulo:(NSString *)titulo conteudo:(NSString *)conteudo
{
    self = [super init];
    if (self) {
        _titulo = titulo;
        _conteudo = conteudo;
    }
    return self;
}

+ (instancetype)notaWithTitulo:(NSString *)titulo conteudo:(NSString *)conteudo
{
    return [[Nota alloc] initWithTitulo:titulo conteudo:conteudo];
}

- (NSString *)description
{
    return self.titulo;
}

@end
