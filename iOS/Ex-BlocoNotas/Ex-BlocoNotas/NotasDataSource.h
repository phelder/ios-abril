//
//  NotasDataSource.h
//  Ex-BlocoNotas
//
//  Created by Helder Pereira on 21/03/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Nota.h"

@interface NotasDataSource : NSObject

@property (strong, nonatomic, readonly) NSMutableArray<Nota *> *notas;

+ (instancetype)sharedDataSource;

@end
