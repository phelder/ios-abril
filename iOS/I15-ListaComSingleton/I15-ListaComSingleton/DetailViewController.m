//
//  DetailViewController.m
//  I15-ListaComSingleton
//
//  Created by Helder Pereira on 10/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "DetailViewController.h"
#import "PessoasDataSource.h"

@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *labelNome;
@property (weak, nonatomic) IBOutlet UILabel *labelIdade;
@property (weak, nonatomic) IBOutlet UILabel *labelCidade;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    PessoasDataSource *pds = [PessoasDataSource sharedDataSource];
    Pessoa *p = pds.gente[self.selectedPosition];
    
    self.labelNome.text = p.nome;
    self.labelIdade.text = p.idade.description;
    self.labelCidade.text = p.cidade;
    
    self.navigationItem.title = p.nome;
}


@end
