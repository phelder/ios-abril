//
//  Pessoa.h
//  I15-ListaComSingleton
//
//  Created by Helder Pereira on 10/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pessoa : NSObject

@property (strong, nonatomic) NSString *nome;
@property (strong, nonatomic) NSNumber *idade;
@property (strong, nonatomic) NSString *cidade;

- (instancetype)initWithNome:(NSString *)nome idade:(NSNumber *)idade cidade:(NSString *)cidade;

+ (instancetype)pessoaWithNome:(NSString *)nome idade:(NSNumber *)idade cidade:(NSString *)cidade;

@end
