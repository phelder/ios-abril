//
//  PessoasDataSource.m
//  I15-ListaComSingleton
//
//  Created by Helder Pereira on 10/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "PessoasDataSource.h"

@implementation PessoasDataSource

+ (instancetype)sharedDataSource {
    
    static PessoasDataSource *instance = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        instance = [[PessoasDataSource alloc] init];
        
        [instance popularArray];
    });
    
    return instance;
    
}

- (void)popularArray {
    
    _gente = @[
               [Pessoa pessoaWithNome:@"Helder" idade:@(35) cidade:@"Porto"],
               [Pessoa pessoaWithNome:@"Marc" idade:@(36) cidade:@"Porto"],
               [Pessoa pessoaWithNome:@"Vanessa" idade:@(24) cidade:@"Coimbra"],
               [Pessoa pessoaWithNome:@"Luís" idade:@(24) cidade:@"Amarante"],
               [Pessoa pessoaWithNome:@"Ricardo Silva" idade:@(31) cidade:@"Aveiro"],
               [Pessoa pessoaWithNome:@"Rui" idade:@(28) cidade:@"Famalicão"],
               [Pessoa pessoaWithNome:@"Ricardo Cardoso" idade:@(35) cidade:@"Aveiro"]
               ];
    
}

@end
