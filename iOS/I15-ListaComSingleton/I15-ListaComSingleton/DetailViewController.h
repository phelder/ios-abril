//
//  DetailViewController.h
//  I15-ListaComSingleton
//
//  Created by Helder Pereira on 10/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (nonatomic) NSInteger selectedPosition;

@end
