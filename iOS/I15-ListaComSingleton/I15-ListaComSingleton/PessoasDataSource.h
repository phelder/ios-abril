//
//  PessoasDataSource.h
//  I15-ListaComSingleton
//
//  Created by Helder Pereira on 10/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

@interface PessoasDataSource : NSObject

@property (strong, nonatomic, readonly) NSArray<Pessoa *> *gente;

+ (instancetype)sharedDataSource;

@end
