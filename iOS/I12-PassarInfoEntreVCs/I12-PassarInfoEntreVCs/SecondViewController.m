//
//  SecondViewController.m
//  I12-PassarInfoEntreVCs
//
//  Created by Helder Pereira on 07/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@property (weak, nonatomic) IBOutlet UILabel *labelShowText;

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.labelShowText.text = self.texto1;
}

- (IBAction)clickedBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
