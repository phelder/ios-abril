//
//  MainViewController.m
//  I12-PassarInfoEntreVCs
//
//  Created by Helder Pereira on 07/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "MainViewController.h"
#import "SecondViewController.h"

@interface MainViewController ()

@property (weak, nonatomic) IBOutlet UITextField *textField1;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - UIButton Actions

- (IBAction)clickedGotoSecond:(id)sender {
    [self performSegueWithIdentifier:@"MainToSecond" sender:nil];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"MainToSecond"]) {
        
        SecondViewController *svc =  segue.destinationViewController;
        
        svc.texto1 = self.textField1.text;
        
    }
    
    
}


@end
