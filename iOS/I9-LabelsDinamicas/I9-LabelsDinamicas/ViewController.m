//
//  ViewController.m
//  I9-LabelsDinamicas
//
//  Created by Helder Pereira on 05/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *label1;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)clickedButton:(id)sender {
    
    self.label1.text = [self.label1.text stringByAppendingString:@"MAIS"];
    
}

@end
