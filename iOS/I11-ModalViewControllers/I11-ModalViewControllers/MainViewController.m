//
//  MainViewController.m
//  I11-ModalViewControllers
//
//  Created by Helder Pereira on 07/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)clickedGotoThird:(id)sender {
    
    [self performSegueWithIdentifier:@"MainToThird" sender:nil];
    
}

@end
