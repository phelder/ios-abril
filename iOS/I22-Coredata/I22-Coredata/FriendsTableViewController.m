//
//  FriendsTableViewController.m
//  I22-Coredata
//
//  Created by Helder Pereira on 17/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "FriendsTableViewController.h"
#import "AppDelegate.h"
#import "Friend.h"
#import "FriendDetailViewController.h"

@interface FriendsTableViewController ()

@end

@implementation FriendsTableViewController
{
    NSArray<Friend *> *_friends;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    
    
    [((UITableView *)self.view) reloadData];
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Friend"];
    
    _friends = [context executeFetchRequest:request error:nil];
    
    return _friends.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell"];
    
    cell.textLabel.text = _friends[indexPath.row].name;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Friend *selectedFriend = _friends[indexPath.row];
    
    [self performSegueWithIdentifier:@"FriendsTableToFriendDetail" sender:selectedFriend];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


#warning APAGUEI O REGISTO MAS A FOTO FICOU... SAFEM-SE
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        NSManagedObjectContext *context = appDelegate.managedObjectContext;
        
        [context deleteObject:_friends[indexPath.row]];
        [appDelegate saveContext];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"FriendsTableToFriendDetail"]) {
        
        if (sender != nil) {
            
            FriendDetailViewController *fdvc = segue.destinationViewController;
            
            fdvc.myFriend = sender;
            
        }
    }
}


#pragma mark - UIButton Actions

- (IBAction)clickedAdd:(id)sender {
    
    [self performSegueWithIdentifier:@"FriendsTableToFriendDetail" sender:nil];
    
}


@end
