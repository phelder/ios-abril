//
//  FriendDetailViewController.m
//  I22-Coredata
//
//  Created by Helder Pereira on 17/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "FriendDetailViewController.h"
#import "AppDelegate.h"
#import "Friend.h"

@interface FriendDetailViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *textFieldName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldAge;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPhoto;
@property (weak, nonatomic) IBOutlet UISwitch *switchImaginary;

@property (weak, nonatomic) IBOutlet UIButton *buttonTakePicture;

@end

@implementation FriendDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.buttonTakePicture.enabled = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    
    if (self.myFriend) {
        
        self.textFieldName.text = _myFriend.name;
        self.textFieldAge.text = _myFriend.age.description;
        self.switchImaginary.on = _myFriend.imaginary.boolValue;
        
        [self loadPicture];
    }
}



- (IBAction)clickedTakePicture:(id)sender {
    
    [self getPictureFromSource:UIImagePickerControllerSourceTypeCamera];
}

- (IBAction)clickedChoosePicture:(id)sender {
    [self getPictureFromSource:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (IBAction)clickedSave:(id)sender {
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    
    if (self.myFriend == nil) {
        self.myFriend = [NSEntityDescription insertNewObjectForEntityForName:@"Friend" inManagedObjectContext:context];
    }
    
    self.myFriend.name = self.textFieldName.text;
    self.myFriend.age = @(self.textFieldAge.text.integerValue);
    self.myFriend.imaginary = @(self.switchImaginary.isOn);
    
    
    if (self.imageViewPhoto.image != nil) {
        NSString *fileName = [NSString stringWithFormat:@"%@.png", [NSDate date].description];
    
        [self savePictureWithName:fileName];
        self.myFriend.photo = fileName;
    }
    
    [appDelegate saveContext];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)loadPicture {
   
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    NSString *filePath = [appDelegate.applicationDocumentsDirectory.path stringByAppendingPathComponent:[NSString stringWithFormat:@"Photos/%@", self.myFriend.photo]];
    
    self.imageViewPhoto.image = [UIImage imageWithContentsOfFile:filePath];
}

- (void)savePictureWithName:(NSString *)fileName {
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    NSData *imageData = UIImagePNGRepresentation(self.imageViewPhoto.image);
    
    NSString *filePath = [appDelegate.applicationDocumentsDirectory.path stringByAppendingPathComponent:[NSString stringWithFormat:@"Photos/%@", fileName]];
    
    [imageData writeToFile:filePath atomically:YES];
    
}

- (void)getPictureFromSource:(UIImagePickerControllerSourceType) sourceType {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.allowsEditing = YES;
    picker.sourceType = sourceType;
    picker.delegate = self;
    
    [self presentViewController:picker animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    self.imageViewPhoto.image = info[UIImagePickerControllerEditedImage];
    
}

@end
