//
//  FriendDetailViewController.h
//  I22-Coredata
//
//  Created by Helder Pereira on 17/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Friend;

@interface FriendDetailViewController : UIViewController

@property (strong, nonatomic) Friend *myFriend;

@end
