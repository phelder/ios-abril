//
//  Friend+CoreDataProperties.m
//  I22-Coredata
//
//  Created by Helder Pereira on 17/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Friend+CoreDataProperties.h"

@implementation Friend (CoreDataProperties)

@dynamic name;
@dynamic imaginary;
@dynamic age;
@dynamic photo;

@end
