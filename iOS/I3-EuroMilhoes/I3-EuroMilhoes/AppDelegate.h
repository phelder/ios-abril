//
//  AppDelegate.h
//  I3-EuroMilhoes
//
//  Created by Helder Pereira on 30/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

