//
//  ViewController.m
//  I3-EuroMilhoes
//
//  Created by Helder Pereira on 30/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"
#import "EuroMilhoes.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *labelChave;
@property (weak, nonatomic) IBOutlet UIView *viewNumbersContainer;
@property (weak, nonatomic) IBOutlet UIView *viewEstrelasContainer;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self gerarLabelsNumeros];
    [self gerarLabelsEstrelas];
}

- (void)gerarLabelsNumeros {
    
    for (int i = 0; i < 50; i++) {
        
        UILabel *label = [[UILabel alloc] init];
        
        int x = i % 10 * (30 + 5) + 5;
        int y = i / 10 * (30 + 5) + 5;
        
        label.frame = CGRectMake(x, y, 30, 30);
        label.backgroundColor = [UIColor redColor];
        label.text = [NSString stringWithFormat:@"%d", i+1];
        
        label.textAlignment = NSTextAlignmentCenter;
        
        label.tag = i + 1;
        
        [self.viewNumbersContainer addSubview:label];
    }
    
}

- (void)gerarLabelsEstrelas {
    
    for (int i = 0; i < 11; i++) {
        
        UILabel *label = [[UILabel alloc] init];
        
        int x = i * (27 + 5) + 5;
        int y = 5;
        
        label.frame = CGRectMake(x, y, 27, 27);
        label.backgroundColor = [UIColor redColor];
        label.text = [NSString stringWithFormat:@"%d", i+1];
        
        label.textAlignment = NSTextAlignmentCenter;
        
        label.tag = i + 1;
        
        [self.viewEstrelasContainer addSubview:label];
    }
    
}

- (void)resetLabelColors {
    NSArray <UILabel *> *todasLabelsNumeros = self.viewNumbersContainer.subviews;
    
    NSArray <UILabel *> *todasLabelsEstrelas = self.viewEstrelasContainer.subviews;
    
    for (int i = 0; i < todasLabelsNumeros.count; i++) {
        todasLabelsNumeros[i].backgroundColor = [UIColor redColor];
    }
    
    for (int i = 0; i < todasLabelsEstrelas.count; i++) {
        todasLabelsEstrelas[i].backgroundColor = [UIColor redColor];
    }
}

- (IBAction)clickedGerarNovaChave:(id)sender {
    // VOLTA A COLOCAR TODOS AS LABELS SEM COR
    [self resetLabelColors];
 
    // CRIA NOVO OBJECTO COM NOVA CHAVE
    EuroMilhoes *em1 = [[EuroMilhoes alloc] init];
    
    // COLOCA STRING CHAVE NA LABEL
    self.labelChave.text = em1.chave;
    
    // ACENDE OS NUMEROS ESCOLHIDOS
    for (int i = 0; i < em1.numeros.count; i++) {
    
        int value = em1.numeros[i].intValue;
        UILabel *label = [self.viewNumbersContainer viewWithTag:value];
        label.backgroundColor = [UIColor greenColor];
    
    }
    
    // ACENDE AS ESTRELAS ESCOLHIDAS
    for (int i = 0; i < em1.estrelas.count; i++) {
        
        int value = em1.estrelas[i].intValue;
        UILabel *label = [self.viewEstrelasContainer viewWithTag:value];
        label.backgroundColor = [UIColor greenColor];
        
    }
}


@end
