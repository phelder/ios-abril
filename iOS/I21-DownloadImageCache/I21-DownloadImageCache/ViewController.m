//
//  ViewController.m
//  I21-DownloadImageCache
//
//  Created by Helder Pereira on 17/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"
#import <UIImageView+AFNetworking.h>

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView1;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)clickedDownloadImage:(id)sender {
    
    NSURL *url = [NSURL URLWithString:@"http://vignette2.wikia.nocookie.net/glee/images/3/33/Homer.png/revision/latest?cb=20120416215731"];
    
    [self.imageView1 setImageWithURL:url];
    
}

@end
