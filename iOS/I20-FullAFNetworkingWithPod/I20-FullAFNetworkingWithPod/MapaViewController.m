//
//  MapaViewController.m
//  I20-FullAFNetworkingWithPod
//
//  Created by Helder Pereira on 14/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "MapaViewController.h"
#import <MapKit/MapKit.h>
#import "DataSource.h"

@interface MapaViewController () <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapa1;

@end

@implementation MapaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mapa1.delegate = self;
    
    DataSource *ds = [DataSource defaultDataSource];
    
    for (User *user in ds.users) {
    
        MKPointAnnotation *pin1 = [[MKPointAnnotation alloc] init];
        
        pin1.coordinate = user.address.geo.coordinate;
        pin1.title = user.username;
        
        [self.mapa1 addAnnotation:pin1];
    }
    
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    //NSLog(@"TESTE");
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {

    MKAnnotationView *v = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:nil];
    
    v.tintColor = [UIColor greenColor];
    
    NSLog(@"%@", annotation);
    
    return v;
}

@end
