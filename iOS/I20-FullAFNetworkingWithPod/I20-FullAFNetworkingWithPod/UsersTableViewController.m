//
//  UsersTableViewController.m
//  I20-FullAFNetworkingWithPod
//
//  Created by Helder Pereira on 14/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "UsersTableViewController.h"
#import "DataSource.h"

@interface UsersTableViewController () <DataSourceDelegate>

@end

@implementation UsersTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadUsers];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
  
    [self.refreshControl addTarget:self
                            action:@selector(loadUsers)
                  forControlEvents:UIControlEventValueChanged];
}

- (void)loadUsers {
    
    self.navigationItem.title = @"Loading...";
    
    DataSource *ds = [DataSource defaultDataSource];
    ds.delegate = self;
    
    [ds loadUsersFrom:@"http://jsonplaceholder.typicode.com/users"];
}

- (void)dataSourceLoadedWithSuccess {
    
    [self.refreshControl endRefreshing];
    
    self.navigationItem.title = @"Users";
    
    [((UITableView *)self.view) reloadData];
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    DataSource *ds = [DataSource defaultDataSource];
    
    return ds.users.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell"];
    
    DataSource *ds = [DataSource defaultDataSource];
    
    User *user = ds.users[indexPath.row];
    
    cell.textLabel.text = user.username;
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
