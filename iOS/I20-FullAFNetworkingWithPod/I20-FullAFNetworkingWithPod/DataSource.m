//
//  DataSource.m
//  I20-FullAFNetworkingWithPod
//
//  Created by Helder Pereira on 14/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "DataSource.h"
#import <AFNetworking.h>


@implementation DataSource

+ (instancetype)defaultDataSource {

    static DataSource *instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        instance = [[DataSource alloc] init];
        
    });

    return instance;
}

- (void)loadUsersFrom:(NSString *)endpoint {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:endpoint
      parameters:nil
        progress:nil
         success:^(NSURLSessionDataTask *task, id responseObject) {
             
             [self parseUserInfo:responseObject];
             
             if ([self.delegate respondsToSelector:@selector(dataSourceLoadedWithSuccess)]) {
                 
                 [self.delegate dataSourceLoadedWithSuccess];
             }
         }
         failure:^(NSURLSessionDataTask *task, NSError *error) {
             
             if ([self.delegate respondsToSelector:@selector(dataSourceError:)]) {
                 
                 [self.delegate dataSourceError:error];
             }
         }];
}

- (void)parseUserInfo:(NSArray *)response {
    
    NSMutableArray *users = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < response.count; i++) {
        
        // CREATE USER INSTANCE
        User *user = [[User alloc] init];
        
        // POPULATE USER FIELDS
        user.webId = response[i][@"id"];
        user.name = response[i][@"name"];
        user.username = response[i][@"username"];
        user.email = response[i][@"email"];
        user.phone = response[i][@"phone"];
        user.website = response[i][@"website"];
        
        // CREATE ADDRESS INSTANCE
        user.address = [[UserAddress alloc] init];
        
        // POPULATE USER ADDRESS FIELDS
        user.address.street = response[i][@"address"][@"street"];
        user.address.suite = response[i][@"address"][@"suite"];
        user.address.city = response[i][@"address"][@"city"];
        user.address.zipcode = response[i][@"address"][@"zipcode"];
        
        NSNumber *lat = response[i][@"address"][@"geo"][@"lat"];
        NSNumber *lng = response[i][@"address"][@"geo"][@"lng"];
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:lat.doubleValue longitude:lng.doubleValue];
        
        user.address.geo = location;
        
        // CREATE COMPANY INSTANCE
        user.company = [[Company alloc] init];
        
        // POPULATE USER COMPANY FIELDS
        user.company.name = response[i][@"company"][@"name"];
        user.company.catchPhrase = response[i][@"company"][@"catchPhrase"];
        user.company.bs = response[i][@"company"][@"bs"];
        
        // ADD USER TO ARRAY BEFORE ITERATING TO NEXT USER IN 'FOR'
        [users addObject:user];
    }
    
    _users = [NSArray arrayWithArray:users];
}

@end
