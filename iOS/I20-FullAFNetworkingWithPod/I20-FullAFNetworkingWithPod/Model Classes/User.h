//
//  User.h
//  I19-AFNetworkingSourceCode
//
//  Created by Helder Pereira on 14/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserAddress.h"
#import "Company.h"

@interface User : NSObject

@property (strong, nonatomic) NSNumber *webId;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) UserAddress *address;
@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) NSString *website;
@property (strong, nonatomic) Company *company;

@end
