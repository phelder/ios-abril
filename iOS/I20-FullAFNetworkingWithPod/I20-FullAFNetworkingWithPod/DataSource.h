//
//  DataSource.h
//  I20-FullAFNetworkingWithPod
//
//  Created by Helder Pereira on 14/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"


@protocol DataSourceDelegate <NSObject>

@optional

- (void)dataSourceLoadedWithSuccess;
- (void)dataSourceError:(NSError *)error;

@end


@interface DataSource : NSObject

@property (weak, nonatomic) id<DataSourceDelegate> delegate;

@property (strong, nonatomic, readonly) NSArray <User *> *users;

+ (instancetype)defaultDataSource;

- (void)loadUsersFrom:(NSString *)endpoint;

@end
