//
//  ViewController.m
//  I4-Delegacao
//
//  Created by Helder Pereira on 03/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIAlertViewDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickedChamaAlerta:(id)sender {
    
    UIAlertView *alerta = [[UIAlertView alloc] init];
    
    alerta.title = @"MUDA COR!!!!";
    alerta.message = @"ESCOLHE A NOVA COR DE FUNDO";
    
    [alerta addButtonWithTitle:@"Cancelar"];
    [alerta addButtonWithTitle:@"Vermelho"];
    [alerta addButtonWithTitle:@"Verde"];
    [alerta addButtonWithTitle:@"Azul"];
    [alerta addButtonWithTitle:@"Branco"];
    [alerta addButtonWithTitle:@"Preto"];
    
    alerta.cancelButtonIndex = 0;
    
    alerta.delegate = self;
    
    [alerta show];
}

- (void)willPresentAlertView:(UIAlertView *)alertView
{
    NSLog(@"CA VEM O ALERTA");
}

- (void)didPresentAlertView:(UIAlertView *)alertView
{
    NSLog(@"CA ESTA O ALERTA");
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 1:
            self.view.backgroundColor = [UIColor redColor];
            break;
        case 2:
            self.view.backgroundColor = [UIColor greenColor];
            break;
        case 3:
            self.view.backgroundColor = [UIColor blueColor];
            break;
        case 4:
            self.view.backgroundColor = [UIColor whiteColor];
            break;
        case 5:
            self.view.backgroundColor = [UIColor blackColor];
            break;
        default:
            break;
    }
    
}


@end
