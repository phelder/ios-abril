//
//  ViewController.m
//  I6-TableViewStoryboard
//
//  Created by Helder Pereira on 03/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation ViewController
{
    NSArray<NSString *> *_nomes;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _nomes = @[
               @"Nome 1",
               @"Nome 2",
               @"Nome 3",
               @"Nome 4",
               @"Nome 5",
               @"Nome 6",
               @"Nome 7",
               @"Nome 8",
               @"Nome 9",
               @"Nome 10",
               @"Nome 11"
               ];
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _nomes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell"];
    
    cell.textLabel.text = _nomes[indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UIAlertView *alerta = [[UIAlertView alloc] init];
    alerta.title = _nomes[indexPath.row];
    
    [alerta addButtonWithTitle:@"OK"];
    
    [alerta show];
}

@end
