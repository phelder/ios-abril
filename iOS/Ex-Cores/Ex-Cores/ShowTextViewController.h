//
//  ShowTextViewController.h
//  Ex-Cores
//
//  Created by Helder Pereira on 07/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowTextViewController : UIViewController

@property (strong, nonatomic) NSString *texto1;
@property (strong, nonatomic) UIColor *cor1;

@end
