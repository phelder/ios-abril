//
//  DownloadImageViewController.m
//  I18-Internet
//
//  Created by Helder Pereira on 14/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "DownloadImageViewController.h"

@interface DownloadImageViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView1;

@end

@implementation DownloadImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)clickedDownloadImage:(id)sender {
    
    UIButton *btnLoadContent = sender;
    
//    NSURL *url = [NSURL URLWithString:@"https://s-media-cache-ak0.pinimg.com/236x/a0/e8/bd/a0e8bd2e370b9d83a5990dd427f49296.jpg"];
    
    NSURL *url = [NSURL URLWithString:@"http://evolucio-humana.wikispaces.com/file/view/Homersapiens.jpg/152060201/Homersapiens.jpg"];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSData *imageData = [NSData dataWithContentsOfURL:url];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.imageView1.image = [UIImage imageWithData:imageData];
            
            btnLoadContent.enabled = YES;
        });
    });
    
    
   // [btnLoadContent setTitle:@"Loading..." forState:UIControlStateDisabled];
    btnLoadContent.enabled = NO;
}

@end
