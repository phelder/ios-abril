//
//  DownloadTextViewController.m
//  I18-Internet
//
//  Created by Helder Pereira on 14/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "DownloadTextViewController.h"

@interface DownloadTextViewController ()

@property (weak, nonatomic) IBOutlet UITextView *textView1;

@end

@implementation DownloadTextViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)clickedLoadContent:(id)sender {
    
    UIButton *btnLoadContent = sender;
    
    NSURL *url = [NSURL URLWithString:@"http://flag.pt"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    
        NSString *content = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    
        dispatch_async(dispatch_get_main_queue(), ^{
            self.textView1.text = content;
            self.textView1.editable = YES;
            btnLoadContent.enabled = YES;
        });
    });
    
    self.textView1.editable = NO;
    self.textView1.text = @"Loading...";
    btnLoadContent.enabled = NO;
    
}


@end
