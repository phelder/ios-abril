//
//  ViewController.m
//  I16-Camara
//
//  Created by Helder Pereira on 12/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView1;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)clickedChangePicture:(id)sender {
    
    self.imageView1.image = [UIImage imageNamed:@"HomerWhooHoo"];
    
}

- (IBAction)clickedTakePicture:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
    }
    
    picker.allowsEditing = YES;
    picker.delegate = self;
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    self.imageView1.image = info[UIImagePickerControllerEditedImage];
    
    NSLog(@"%@", info);
}

- (IBAction)clickedSaveToDisk:(id)sender {
    
    // DOCUMENTOS DA APP (READ-WRITE)
    // CONTAINER (READ)
    
    NSFileManager *fileMan = [NSFileManager defaultManager];
    
    NSURL *docsURL = [fileMan URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask][0];
    
    NSString *docsPath = docsURL.path;
    
    NSString *newFilePath = [docsPath stringByAppendingPathComponent:@"imagem1.png"];
    
    NSLog(@"%@", docsPath);
    
    NSData *imageData = UIImagePNGRepresentation(self.imageView1.image);
    
    [imageData writeToFile:newFilePath atomically:YES];

}

- (IBAction)clickedLoadFromDisk:(id)sender {
    
    NSFileManager *fileMan = [NSFileManager defaultManager];
    
    NSURL *docsURL = [fileMan URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask][0];
    
    NSString *docsPath = docsURL.path;
    
    NSString *newFilePath = [docsPath stringByAppendingPathComponent:@"imagem1.png"];
    
    
    self.imageView1.image = [UIImage imageWithContentsOfFile:newFilePath];
}

@end
