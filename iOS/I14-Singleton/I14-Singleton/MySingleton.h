//
//  MySingleton.h
//  I14-Singleton
//
//  Created by Helder Pereira on 10/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MySingleton : NSObject

@property (strong, nonatomic) NSString *textoGlobal;
@property (strong, nonatomic) NSArray *arrayGlobal;

+ (instancetype)sharedInstance;

@end
