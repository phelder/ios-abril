//
//  MainViewController.m
//  I14-Singleton
//
//  Created by Helder Pereira on 10/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "MainViewController.h"
#import "MySingleton.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)clickedFastForward:(id)sender {
 
    MySingleton *ms = [MySingleton sharedInstance];
    ms.textoGlobal = @"YAY EU EXISTO EM TODO O LADO...";
    
    [self performSegueWithIdentifier:@"MainToSecond" sender:nil];
    
}


@end
