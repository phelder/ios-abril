//
//  MySingleton.m
//  I14-Singleton
//
//  Created by Helder Pereira on 10/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "MySingleton.h"

@implementation MySingleton

+ (void)teste {
    static int contador = 0;
    contador ++;
    NSLog(@"contador: %d", contador);
}

+ (instancetype)sharedInstance {
    
    static MySingleton *instance = nil;
    
    if (instance == nil) {
        instance = [[MySingleton alloc] init];
    }
    
    return instance;
}

@end
