//
//  SecondViewController.m
//  I14-Singleton
//
//  Created by Helder Pereira on 10/05/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "SecondViewController.h"
#import "MySingleton.h"

@interface SecondViewController ()

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    MySingleton *ms = [MySingleton sharedInstance];
    
    NSLog(@"TEXTO: %@", ms.textoGlobal);
}

@end
