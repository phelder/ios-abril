//
//  pessoa.c
//  C17-CrazyPointers
//
//  Created by Helder Pereira on 16/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include "pessoa.h"

Pessoa criaPessoa(const char *nome, int idade) {

    Pessoa p;
    p.nome = nome;
    p.idade = idade;

    return p;
}