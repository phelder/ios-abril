//
//  main.c
//  C17-CrazyPointers
//
//  Created by Helder Pereira on 16/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "pessoa.h"

void exemplo1() {
    
    // CRIA ARRAY
    Pessoa gente[5] = {
        criaPessoa("Tobias", 20),
        criaPessoa("Malaquias", 15),
        criaPessoa("Zacarias", 18),
        criaPessoa("Matias", 22),
        criaPessoa("Farias", 19)
    };
    
    // LISTA GENTE
    for (int i = 0; i < 5; i++) {
        printf("%d: %s, %d anos\n", i, gente[i].nome, gente[i].idade);
    }
    
    // CALCULA MV MN SOMA MEDIA
    
    int indiceMV = 0;
    Pessoa maisNova = gente[0];
    
    int soma = 0;
    
    for (int i = 0; i < 5; i++) {
        if (gente[i].idade > gente[indiceMV].idade) {
            indiceMV = i;
        }
        
        if (gente[i].idade < maisNova.idade) {
            maisNova = gente[i];
        }
        
        soma += gente[i].idade;
    }
    double media = soma / 5.0;
    
    // VAMOS IMPRIMIR
    
    printf("MV: %s, %d anos\n", gente[indiceMV].nome, gente[indiceMV].idade);
    printf("MN: %s, %d anos\n", maisNova.nome, maisNova.idade);
    printf("SOMA: %d\n", soma);
    printf("MEDIA: %f\n", media);
    
}

void exemplo2() {
    
    typedef struct {
        
        char *nome;
        int idade;
        
    } Pessoa;
    
    Pessoa gente[5];
    
    for (int i = 0; i < 5; i++) {
        printf("Introduza o nome da %dª pessoa:\n>", i+1);
        
        char inputNome[10];
        fgets(inputNome, 10, stdin);
        inputNome[strlen(inputNome)-1] = '\0';
        
        gente[i].nome = malloc(10);
        strcpy(gente[i].nome, inputNome);
        
        printf("Introduza a idade da %dª pessoa:\n>", i+1);
        int inputIdade;
        scanf("%d", &inputIdade);
        gente[i].idade = inputIdade;
        
        // LIMPAR O BUFFER
        fseek(stdin, 0, SEEK_END);
    }
    
    // LISTA GENTE
    for (int i = 0; i < 5; i++) {
        printf("%d: %s, %d anos\n", i, gente[i].nome, gente[i].idade);
        //printf("%d: %d anos\n", i, gente[i].idade);
    }
    
    // LIBERTAR ALOCS
    for (int i = 0; i < 5; i++) {
        free(gente[i].nome);
    }

}

void exemplo3() {
    typedef struct {
        
        char nome[10];
        int idade;
        
    } Pessoa;
    
    Pessoa gente[5];
    
    for (int i = 0; i < 5; i++) {
        printf("Introduza o nome da %dª pessoa:\n>", i+1);
        
        char inputNome[10];
        fgets(inputNome, 10, stdin);
        inputNome[strlen(inputNome)-1] = '\0';
        
        strcpy(gente[i].nome, inputNome);
        
        printf("Introduza a idade da %dª pessoa:\n>", i+1);
        int inputIdade;
        scanf("%d", &inputIdade);
        gente[i].idade = inputIdade;
        
        // LIMPAR O BUFFER
        fseek(stdin, 0, SEEK_END);
    }
    
    // LISTA GENTE
    for (int i = 0; i < 5; i++) {
        printf("%d: %s, %d anos\n", i, gente[i].nome, gente[i].idade);
        //printf("%d: %d anos\n", i, gente[i].idade);
    }
}

void exemplo4() {
    typedef struct {
        
        char nome[10];
        int idade;
        
    } Pessoa;
    
    Pessoa *gente = NULL;
    
    int cont = 0;
    
    while (true) {
        printf("Introduza o nome da %dª pessoa (fim para terminar):\n>", cont+1);
        
        char inputNome[10];
        fgets(inputNome, 10, stdin);
        inputNome[strlen(inputNome)-1] = '\0';
        
        if (strcmp(inputNome, "FIM") == 0 || strcmp(inputNome, "fim") == 0) {
            break;
        }
        
        if (cont == 0) {
            gente = malloc(1 * sizeof(Pessoa));
        } else {
            gente = realloc(gente, (cont+1) * sizeof(Pessoa));
        }
        
        strcpy(gente[cont].nome, inputNome);
        
        printf("Introduza a idade da %dª pessoa:\n>", cont+1);
        int inputIdade;
        scanf("%d", &inputIdade);
        gente[cont].idade = inputIdade;
        
        // LIMPAR O BUFFER
        fseek(stdin, 0, SEEK_END);
        
        cont++;
    }
    
    // LISTA GENTE
    for (int i = 0; i < cont; i++) {
        printf("%d: %s, %d anos\n", i, gente[i].nome, gente[i].idade);
    }
    
    if (cont > 0) {
        free(gente);
    }
}

int main(int argc, const char * argv[]) {
    
    exemplo4();
    
    return 0;
}


