//
//  pessoa.h
//  C17-CrazyPointers
//
//  Created by Helder Pereira on 16/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#ifndef pessoa_h
#define pessoa_h

#include <stdio.h>

#endif /* pessoa_h */

typedef struct {
    
    const char *nome;
    int idade;
    
} Pessoa;

Pessoa criaPessoa(const char *nome, int idade);