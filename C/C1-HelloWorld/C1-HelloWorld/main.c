//
//  main.c
//  C1-HelloWorld
//
//  Created by Helder Pereira on 07/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {
    
    printf("Hello, World!\n");
    
    int a = 12;
    int b = 5;
    
    int soma = a + b;
    int subt = a - b;
    int mult = a * b;
    float divi = (float)a / b;
    int rest = a % b;
    
    // +  -  *  /  %
    
    printf("A soma de %d com %d é %d\n", a, b, soma);
    printf("%d + %d = %d\n", a, b, soma);
    printf("%d - %d = %d\n", a, b, subt);
    printf("%d X %d = %d\n", a, b, mult);
    printf("%d / %d = %.2f\n", a, b, divi);
    printf("%d %% %d = %d\n", a, b, rest);
    
    
    return 0;
}
