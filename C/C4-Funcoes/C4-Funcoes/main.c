//
//  main.c
//  C4-Funcoes
//
//  Created by Helder Pereira on 07/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>

void teste() {
    
    printf("EU SOU UM TESTE\n");
    
}

int retornaUmValor(int num) {
    
    teste();
    
    return num * num;
    
}

int main(int argc, const char * argv[]) {

    int resultado = retornaUmValor(8);
    
    printf("%d\n", resultado);
    
    return 0;
}