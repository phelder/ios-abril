//
//  rects.h
//  C10-MultiplosFicheiros
//
//  Created by Helder Pereira on 09/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#ifndef rects_h
#define rects_h

#include <stdio.h>

#endif /* rects_h */

typedef struct {
    int width;
    int height;
} Rect;


Rect makeRect(int width, int height);

int calcRectArea(Rect r);