//
//  main.c
//  C10-MultiplosFicheiros
//
//  Created by Helder Pereira on 09/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include "rects.h"

#define NUM_RECTS 10

int main(int argc, const char * argv[]) {
    
    Rect rects[NUM_RECTS];
    
    for (int i = 0; i < NUM_RECTS; i++) {
        
        rects[i] = makeRect(4, 4);
        
    }

    
    
    return 0;
}

