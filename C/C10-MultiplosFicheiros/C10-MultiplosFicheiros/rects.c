//
//  rects.c
//  C10-MultiplosFicheiros
//
//  Created by Helder Pereira on 09/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include "rects.h"

Rect makeRect(int width, int height) {
    Rect r;
    r.width = width;
    r.height = height;
    return r;
}

int calcRectArea(Rect r) {
    return r.width * r.height;
}