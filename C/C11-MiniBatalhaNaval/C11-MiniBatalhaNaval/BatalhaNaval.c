//
//  BatalhaNaval.c
//  C11-MiniBatalhaNaval
//
//  Created by Helder Pereira on 12/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include "BatalhaNaval.h"

void resetMatrizJogo(char matrizJogo[TAM_MATRIZ][TAM_MATRIZ]) {
    
    for (int i = 0; i < TAM_MATRIZ; i++) {
        for (int j = 0; j < TAM_MATRIZ; j++) {
            matrizJogo[i][j] = '.';
        }
    }
}

void imprimirMatrizJogo(char matrizJogo[TAM_MATRIZ][TAM_MATRIZ]) {
    
    printf(" ");
    for (int i = 0; i < TAM_MATRIZ; i++) {
        printf(" %d", i);
    }
    printf("\n");
    for (int i = 0; i < TAM_MATRIZ; i++) {
        
        printf("%d ", i);
        
        for (int j = 0; j < TAM_MATRIZ; j++) {
            printf("%c ", matrizJogo[i][j]);
        }
        printf("\n");
    }
}