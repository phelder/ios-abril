//
//  BatalhaNaval.h
//  C11-MiniBatalhaNaval
//
//  Created by Helder Pereira on 12/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#ifndef BatalhaNaval_h
#define BatalhaNaval_h

#include <stdio.h>

#endif /* BatalhaNaval_h */

#define TAM_MATRIZ 10

void resetMatrizJogo(char matrizJogo[TAM_MATRIZ][TAM_MATRIZ]);
void imprimirMatrizJogo(char matrizJogo[TAM_MATRIZ][TAM_MATRIZ]);
