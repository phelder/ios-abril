//
//  main.c
//  C11-MiniBatalhaNaval
//
//  Created by Helder Pereira on 09/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <stdbool.h>
#include "BatalhaNaval.h"

int main(int argc, const char * argv[]) {
    
    int matrizSecreta[10][10] = {
        { 2, 2, 0, 3, 0, 0, 4, 4, 4, 4 },
        { 0, 0, 0, 3, 0, 0, 0, 0, 0, 0 },
        { 4, 0, 0, 3, 0, 3, 3, 3, 0, 2 },
        { 4, 0, 0, 0, 0, 0, 0, 0, 0, 2 },
        { 4, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 4, 0, 0, 0, 0, 2, 0, 0, 0, 3 },
        { 0, 0, 0, 0, 0, 2, 0, 0, 0, 3 },
        { 2, 2, 0, 0, 0, 0, 0, 0, 0, 3 },
        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 0, 0, 0, 0, 6, 6, 6, 6, 6, 6 }
    };
    
    char matrizJogo[10][10];
    resetMatrizJogo(matrizJogo);
    
    int vidas = 20;
    
    bool ganhou = false;
    int afundados = 0;
    
    while (true) {
        imprimirMatrizJogo(matrizJogo);
        printf("Vidas: %d\n", vidas);
        
        int linha;
        int coluna;
        
        printf("Introduza a linha\n>");
        scanf("%d", &linha);
        
        printf("Introduza a coluna\n>");
        scanf("%d", &coluna);

        if (matrizSecreta[linha][coluna] == 0) {
            matrizJogo[linha][coluna] = 'x';
            vidas--;
            if (vidas == 0) {
                break;
            }
        } else {
            if (matrizJogo[linha][coluna] == '.') {
                afundados++;
            }
            matrizJogo[linha][coluna] = matrizSecreta[linha][coluna] + 48;
        }
        
        if (afundados == 31) {
            ganhou = true;
            break;
        }
    
    }
    
    if (ganhou) {
        printf("GANHOU!!!!\n");
    } else {
        printf("NABO!!!!\n");
    }
    
    
    
    return 0;
}
