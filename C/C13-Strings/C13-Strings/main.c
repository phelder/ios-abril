//
//  main.c
//  C13-Strings
//
//  Created by Helder Pereira on 14/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <string.h>

int main(int argc, const char * argv[]) {
    
    char texto1[4599] = "Ola";
    char texto2[10] = { 'O', 'l', 97, 0 };
    
    printf("TEXTO 1: %s\n", texto1);
    printf("TEXTO 2: %s\n", texto2);
    
    printf("TAMANHO TEXTO 1: %lu\n", strlen(texto1));
    printf("TAMANHO TEXTO 2: %lu\n", strlen(texto2));
    
    if (strcmp(texto1, texto2) == 0) {
        printf("SAO IGUAIS\n");
    } else {
        printf("NAO SAO IGUAIS\n");
    }
    
    /* -- PALINDROMO -- */
    
    
    char palavra[100];
    char palavraInvertida[100];
    
    printf("Introduza uma palavra:\n>");
    scanf("%s", palavra);
    
    int tamanho = (int)strlen(palavra);
    
    for (int i = 0; i < tamanho; i++) {
        
        int j = tamanho - 1 -i;
        
        palavraInvertida[i] = palavra[j];
    }
    palavraInvertida[tamanho] = '\0';
    
    printf("%s\n", palavra);
    printf("%s\n", palavraInvertida);
    
    if (strcmp(palavra, palavraInvertida) == 0) {
        printf("PALINDROMO YAY!!!!!\n");
    } else {
        printf("nao e palindromo...\n");
    }
    
    return 0;
}
