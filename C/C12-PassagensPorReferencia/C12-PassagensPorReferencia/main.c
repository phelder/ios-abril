//
//  main.c
//  C12-PassagensPorReferencia
//
//  Created by Helder Pereira on 12/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>

void recebeReferencia(int *numPointer) {
    
    //(*numPointer)++;
    
    numPointer[0]++;
}

void recebeValor(int num) {
    
    num++;
    
}

void recebeUmArray(int *array) {

    array[2] = 45;
    array[0] = 100;

}

int main(int argc, const char * argv[]) {
    
//    int a = 20;
//    
//    recebeReferencia(&a);
//    
//    printf("%d\n", a);

    int idades[8] = { 23, 12, 34, 12, 76, 23, 12, 34 };
    
    recebeUmArray(idades);
    
    for (int i = 0; i < 8; i++) {
        printf("%d\n", idades[i]);
    }
    
    return 0;
}
