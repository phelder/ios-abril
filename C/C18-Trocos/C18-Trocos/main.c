//
//  main.c
//  C18-Trocos
//
//  Created by Helder Pereira on 16/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <math.h>

int main(int argc, const char * argv[]) {
    
    float valorTotal = 3.47;
    
    float valorMao = valorTotal;
    
    int c2e = 0;
    int c1e = 0;
    int c50c = 0;
    int c20c = 0;
    int c10c = 0;
    int c5c = 0;
    int c2c = 0;
    int c1c = 0;
    
    while (valorMao > 0.001) {
        if (valorMao > 2) {
            valorMao -= 2;
            c2e++;
            continue;
        }
        
        if (valorMao > 1) {
            valorMao -= 1;
            c1e++;
            continue;
        }
        
        if (valorMao > 0.5) {
            valorMao -= 0.5;
            c50c++;
            continue;
        }
        
        if (valorMao > 0.2) {
            valorMao -= 0.2;
            c20c++;
            continue;
        }
        
        if (valorMao > 0.1) {
            valorMao -= 0.1;
            c10c++;
            continue;
        }
        
        if (valorMao > 0.05) {
            valorMao -= 0.05;
            c5c++;
            continue;
        }
        
        if (valorMao > 0.02) {
            valorMao -= 0.02;
            c2c++;
            continue;
        }
        
        if (valorMao > 0.01) {
            valorMao -= 0.01;
            c1c++;
            continue;
        }
        
    }
    
    printf("%.2f€\n", valorTotal);
    
    printf("2€: %d\n", c2e);
    printf("1€: %d\n", c1e);
    printf("50c: %d\n", c50c);
    printf("20c: %d\n", c20c);
    printf("10c: %d\n", c10c);
    printf("5c: %d\n", c5c);
    printf("2c: %d\n", c2c);
    printf("1c: %d\n", c1c);
    
    return 0;
}
