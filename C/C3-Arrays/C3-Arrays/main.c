//
//  main.c
//  C3-Arrays
//
//  Created by Helder Pereira on 07/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {
    
    int idades[6];
    
    idades[0] = 35;
    idades[1] = 36;
    idades[2] = 24;
    idades[3] = 24;
    idades[4] = 31;
    idades[5] = 28;
    
    int soma = 0;
    int maisVelho = idades[0];
    int maisNovo = idades[0];
    
    for (int i = 0; i < 6; i++) {
        printf("%d\n", idades[i]);
        
        soma = soma + idades[i];
        
        if (idades[i] > maisVelho) {
            maisVelho = idades[i];
        }
        
        if (idades[i] < maisNovo) {
            maisNovo = idades[i];
        }
    }
    
    float media = soma / 6.0;
    
    printf("Soma: %d\n", soma);
    printf("Média: %f\n", media);
    printf("Mais velho: %d\n", maisVelho);
    printf("Mais novo: %d\n", maisNovo);
    
    return 0;
}
