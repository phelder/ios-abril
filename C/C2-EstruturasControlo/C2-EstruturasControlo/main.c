//
//  main.c
//  C2-EstruturasControlo
//
//  Created by Helder Pereira on 07/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <stdbool.h>

int main(int argc, const char * argv[]) {
    
    // ==  !=  >  >=  <  <=
    
    int idade;
    
    printf("Que idade tens?\n>");
    
    scanf("%d", &idade);
    
    if (idade >= 18) {
        printf("Toma lá uma cerveja!\n");
    } else {
        printf("Toma lá uma cola!\n");
    }
    
    // CICLOS WHILE
    
    int numero = 7;
    
    int contador = 0;
    while (contador < 10) {
        contador++;
        
        int res = numero * contador;
        
        printf("%d X %d = %d\n", numero, contador, res);
        
    }
    
    
    
    return 0;
}
