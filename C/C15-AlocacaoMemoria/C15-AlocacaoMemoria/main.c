//
//  main.c
//  C15-AlocacaoMemoria
//
//  Created by Helder Pereira on 14/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>


int main(int argc, const char * argv[]) {
    
    
//    int *numPoint;
//    numPoint = malloc(10 * sizeof(int));
//
//    OU
    
    int *numPoint = malloc(10 * sizeof(int));
    
    numPoint[0] = 23;
    numPoint[1] = 23;
    numPoint[2] = 23;
    numPoint[3] = 23;
    numPoint[4] = 23;
    numPoint[5] = 23;
    numPoint[6] = 23;
    numPoint[7] = 23;
    numPoint[8] = 23;
    numPoint[9] = 23;
    
    numPoint = realloc(numPoint, 12 * sizeof(int));
    
    numPoint[10] = 34;
    numPoint[11] = 34;
    
    for (int i = 0; i < 12; i++) {
        printf("%d: %d\n", i, numPoint[i]);
    }
    
    free(numPoint);
    
    return 0;
}

