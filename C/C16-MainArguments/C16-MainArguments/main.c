//
//  main.c
//  C16-MainArguments
//
//  Created by Helder Pereira on 16/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {
    
    for (int i = 0; i < argc; i++) {
        
        printf("%d: %s\n", i, argv[i]);
        
    }
    
    return 0;
}
