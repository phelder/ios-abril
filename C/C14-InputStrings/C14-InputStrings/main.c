//
//  main.c
//  C14-InputStrings
//
//  Created by Helder Pereira on 14/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <string.h>

int main(int argc, const char * argv[]) {
    
    printf("Introduza uma frase:\n>");
    
    char frase[255];
    fgets(frase, 254, stdin);
    frase[strlen(frase) -1] = '\0';
    
    printf("%s\n", frase);
    
    return 0;
}
