//
//  main.c
//  C6-ExerciciosRandom
//
//  Created by Helder Pereira on 09/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int randomiza(int min, int max) {
    return (rand() % (max + 1 - min)) + min;
}

int main(int argc, const char * argv[]) {

    srand((int)time(NULL));
    
    int numeros[10];
    
    // PREENCHER COM 10 NUMEROS ALEATORIOS ENTRE 1 e 100
    for (int i = 0; i < 10; i++) {
        numeros[i] = randomiza(1, 100);
    }
    
    // IMPRIMIR
    for (int i = 0; i < 10; i++) {
        printf("%d\n", numeros[i]);
    }
    
    
    // AGORA OUTRO
    
//    int contador[6];
//    for (int i = 0; i < 6; i++) {
//        contador[i] = 0;
//    }
//    
//    for (int i = 0; i < 1000; i++) {
//        int n = randomiza(0, 5);
//
//        contador[n]++;
//    }
//    
//    
//    for (int i = 0; i < 6; i++) {
//        printf("%d: %d\n", i, contador[i]);
//    }

    int min = 5;
    int max = 15;
    
    int contador[11];
    for (int i = 0; i < 11; i++) {
        contador[i] = 0;
    }
    
    for (int i = 0; i < 1000; i++) {
        int n = randomiza(min, max);
        
        contador[n - min]++;
    }
    
    for (int i = 0; i < 11; i++) {
        printf("%d: %d\n", i + min, contador[i]);
    }
    
    
    return 0;
}
