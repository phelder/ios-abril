//
//  main.c
//  C19-Fibonacci
//
//  Created by Helder Pereira on 16/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main(int argc, const char * argv[]) {
    
    int fibo20[20];
    
    fibo20[0] = 0;
    fibo20[1] = 1;
    
    for (int i = 2; i < 20; i++) {
        fibo20[i] = fibo20[i-1] + fibo20[i-2];
    }
    
    for (int i = 0; i < 20; i++) {
        printf("%d: %d\n", i + 1, fibo20[i]);
    }
    
    
    
    
    int *fiboN = malloc(2 * sizeof(int));
    fiboN[0] = 0;
    fiboN[1] = 1;
    
    int num = 0;
    int contador = 2;
    while (true) {
        num = fiboN[contador-1] + fiboN[contador-2];
        
        if (num > 10000) {
            break;
        } else {
            fiboN = realloc(fiboN, (contador+1)*sizeof(int));
            fiboN[contador] = num;
        }
        
        contador++;
    }
    
    for (int i = 0; i < contador; i++) {
        printf("%d: %d\n", i + 1, fiboN[i]);
    }
    
    free(fiboN);
    
    return 0;
}
