//
//  main.c
//  C9-Struct
//
//  Created by Helder Pereira on 09/04/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

// FORMA BASE
//struct rectangulo {
//    int altura;
//    int largura;
//};

// COM TYPE DEF
//struct rectangulo {
//    int altura;
//    int largura;
//};
//
//typedef struct rectangulo teste;

// COM TYPE DEF INLINE

typedef struct {
    int altura;
    int largura;
} Rectangulo;

bool eQuadrado(Rectangulo rect) {
    
    return rect.altura == rect.largura;
    
}

int calculaArea(Rectangulo rect) {
    
    return rect.altura * rect.largura;

}

int randomiza(int min, int max) {
    return (rand() % (max + 1 - min)) + min;
}

int main(int argc, const char * argv[]) {
    srand((int)time(NULL));

    
    Rectangulo r1;
    r1.altura = 5;
    r1.largura = 10;
    
    Rectangulo r2;
    r2.altura = 2;
    r2.largura = 8;
    
    Rectangulo r3;
    r3.altura = 4;
    r3.largura = 9;
    
    Rectangulo rects[10];
    for (int i = 0; i < 10; i++) {
        rects[i].altura = randomiza(1, 10);
        rects[i].largura = randomiza(1, 10);
        
        if (eQuadrado(rects[i])) {
            printf("Q");
        } else {
            printf("R");
        }
        printf("- l:%d a:%d area:%d\n", rects[i].largura, rects[i].altura, calculaArea(rects[i]));
    }
    
    Rectangulo rectMaior = rects[0];
    for (int i = 1; i < 10; i++) {
        if (calculaArea(rects[i]) > calculaArea(rectMaior)) {
            rectMaior = rects[i];
        }
    }
    
    printf("MAIOR - l:%d a:%d area:%d\n", rectMaior.largura, rectMaior.altura, calculaArea(rectMaior));
    
    
    return 0;
}
